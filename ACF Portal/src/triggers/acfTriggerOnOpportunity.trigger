trigger acfTriggerOnOpportunity on Opportunity (after insert,after update) 
{
    acfTriggerOnOpportunityHandler objTrgOppHandler = new acfTriggerOnOpportunityHandler();
    if(trigger.isAfter && trigger.isInsert)
      objTrgOppHandler.OnAfterInsert(Trigger.new);
   
}