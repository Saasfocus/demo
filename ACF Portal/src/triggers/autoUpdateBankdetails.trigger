/*
    This trigger is used to update bankdetails when attachment is created
    =============================================================================
    Name                             Date                                version
    =============================================================================
    Karthik Chekkilla                04/02/2015                                1.0
    =============================================================================
*/

trigger autoUpdateBankdetails on Attachment (after insert) 
{

    public list<acfBank_Detail__c> lstBankdetails = new list<acfBank_Detail__c>();
    public list<acfCMS_Page__c> lstCMSPage = new list<acfCMS_Page__c>();
    Map<String,String> objectMap = new Map<String,String>();  
    List<Schema.SObjectType> lstSObject = Schema.getGlobalDescribe().Values();
    for(Schema.SObjectType sObjType : lstSObject)
    {
        objectMap.put(sObjType.getDescribe().getKeyPrefix(), sObjType.getDescribe().getName());
    }
    for(Attachment attchObj : Trigger.New)
    {
       If(attchObj != null && attchObj.ParentId != null)
       { 
        string strTempId = attchObj.ParentId;
        String prefix =  strTempId.substring(0,3);
        string objectAPIName = objectMap.get(prefix);
        System.debug('objectAPIName ----------- '+objectAPIName);
        If(objectAPIName == 'acfBank_Detail__c')
        {
           acfBank_Detail__c objBankdetails = new acfBank_Detail__c(id = attchObj.ParentId,acfAttachmentId__c = attchObj.Id); 
           lstBankdetails.add(objBankdetails);
        }
        //added by karthik on 22-04-2015 for Image URL on CMS page object
        If(objectAPIName == 'acfCMS_Page__c')
        {
           acfCMS_Page__c objCMSPage = new acfCMS_Page__c(id = attchObj.ParentId,acfAttachment_Id__c = attchObj.Id); 
           lstCMSPage.add(objCMSPage);
        }
      }
    }
    If(lstBankdetails != null && lstBankdetails.size()>0)
    {
         Update lstBankdetails;
    }
    if(lstCMSPage != null && lstCMSPage.size()>0)
    {
        update lstCMSPage;
    }
}