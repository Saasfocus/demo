trigger acfAutoUpdateCMS on Attachment (after insert) {
    
    public id id;
    public id parent_id;
    
    public list<acfContentManagementSystem__c> lst_cms_obj = new list<acfContentManagementSystem__c>();
    
    List<Schema.SObjectType> lstSObject = Schema.getGlobalDescribe().Values();
    Map<String,String> objectMap = new Map<String,String>();
    for(Schema.SObjectType sObjType : lstSObject){
        objectMap.put(sObjType.getDescribe().getKeyPrefix(), sObjType.getDescribe().getName());
    }
    
    for(Attachment attch_obj : Trigger.New){
        if(Trigger.isAfter){
            if(Trigger.isInsert){
                string strTempId = attch_obj.ParentId;
                String prefix =  strTempId.substring(0,3);
                string objectAPIName = objectMap.get(prefix);
                system.debug('objectAPIName ----------- '+objectAPIName);
                    
                if(attch_obj.id != null && objectAPIName == 'acfContentManagementSystem__c'){
                       
                    acfContentManagementSystem__c cms_obj = new acfContentManagementSystem__c(id = attch_obj.ParentId, acfAttachmentId__c = attch_obj.id);
                    system.debug('scnd cms_obj --------- '+cms_obj);                        
                    lst_cms_obj.add(cms_obj);
                    system.debug('lst_cms_obj --------- '+lst_cms_obj);
                        
                }  
            }
        }   
    }
       
    try{
        update lst_cms_obj;
    }catch(DMLException e){
    
    }
}