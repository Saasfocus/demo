//Created by karthik
public without sharing class acfFooter 
{
    public acfContentManagementSystem__c cms_obj {get; set;}
    public list<acfContentManagementSystem__c> lstFooterLinks {get;set;}
    public string selectedLinkId {get;set;}
    public Map<string,string> mapCMSpgid {get;set;} 
    public string pge_name {set; get;}
    public acfFooter()
    {
        string pge = Apexpages.currentPage().getUrl();
        List<String> parts = pge.split('/'); 
        List<String> parts1 = parts[2].split('\\?');
        pge_name = parts1[0].toLowerCase(); 
        system.debug('cons pge_name --------'+pge_name);
        
        mapCMSpgid = new map<string,string>();
        cms_obj = [select id,name,acfDescription__c,acfPageName__c,acfSectionName__c from acfContentManagementSystem__c  where acfPageName__c = 'common' and acfSectionName__c = 'Footer' limit 1]; 
        lstFooterLinks = [select id,name,acfDescription__c,acfPageName__c,acfSectionName__c,acfLink_URL__c,acfIs_Checked__c,acfHeading__c,acfSequence__c,acfHeader_Sequance__c,acfCMS_Page__c from acfContentManagementSystem__c  where acfSectionName__c = 'Footer Links' AND acfIs_Checked__c = true order by acfSequence__c];
        for(acfContentManagementSystem__c cmsObj : lstFooterLinks)
        {
            If(cmsObj.acfCMS_Page__c != null)
            {   
             mapCMSpgid.put(cmsObj.id,cmsObj.acfCMS_Page__c); 
            }
        } 
    }
   /* public pagereference redirectCommonFLpage()
    {
        string retURL = '/ACFInfo?id='+mapCMSpgid.get(selectedLinkId);
        if(retURL != null && retURL <> ''){
            pagereference pageref = new pagereference(retURL);
        return pageref;
        }else{
            return null;
        }
    }*/
}