@isTest(seealldata = true)
public class acfCommontracker
{
    private static testMethod void validate()
    {
        User objUser=new User();
        objUser=acfCommonTrackerClass.CreateUser(objUser);
              
        list<string> lstemail = new list<string>();
        lstemail.add('ankit@gmail.com');
        lstemail.add('ankitsingh@gmail.com');
 

        
        Document_Master__c objDocumentMaster = new Document_Master__c();
        objDocumentMaster = acfCommontrackerClass.createDocumentMaster(objDocumentMaster);
        
         Product2 objpro = new Product2();
         objpro = acfCommonTrackerClass.createProduct(objpro);
         
         Question__c  objquestio = new Question__c();
         objquestio  = acfCommonTrackerClass.createQuestion(objquestio);
         
         Answer__c objans = new Answer__c();
         objans.acf_Question__c = objquestio.id;
         objans = acfCommonTrackerClass.createAnswer(objans);
        
        acfDependent_Product__c objdependent = new acfDependent_Product__c();
        objdependent.acfAnswer__c = objans.id;
        objdependent.acfFilter__c = 'Include';
        objdependent.acfProduct__c = objpro.id;
        insert objdependent;
        
        Dependent_Document__c objdep = new Dependent_Document__c();
        objdep.acfAnswer__c = objans.id;
        objdep.acfActive__c = true;
        objdep.acfDocument_Master__c = objDocumentMaster.id;
        objdep.name = 'test';
        insert objdep;
        
        set<string> set1=new set<String>();
        set1.add(string.valueof(objans.id));
        
        Lead objLead = new Lead();
        objLead = acfCommonTrackerClass.createLeadForPortal();
        Account objAccount = new Account();
        objAccount = acfCommonTrackerClass.CreatePersonAccount('test','test','+619540505050','test@fakeemail.com',objLead.id);
        User objUse =new User();
        objUse =  acfCommonTrackerClass.CreatePortalUser(objAccount.id); 
        Opportunity objOpportunity = new Opportunity();
        objOpportunity = acfCommontrackerClass.createOpportunity(objOpportunity,objUse);
        
        Required_Document__c  objRequiredDocument = new Required_Document__c ();      
        objRequiredDocument = acfCommontrackerClass.createRequiredDocument(objRequiredDocument,objDocumentMaster,objOpportunity);
    System.runAs(objUse) 
        {
           acfCommon.sendSMS('Anita','+619873164658','message',true);
           acfCommon.checkEmail('sushree.biswal@saasfocus.com');
           acfCommon.sendTemplatedEmail(objUser.Id,'strContent','strSubject',lstemail,lstemail);
           acfCommon.SendSMSUsingFuture('recepientName','+619873164685','message',true);
           acfCommon.sendReturnUrl(objUse.Id,true,'pgname'); 
           acfCommon.redirect(userinfo.getuserid(),'clickjumiointegration');
           acfCommon.getCurrentLoggedInContactId(objUse.Id); 
           acfCommon.getCurrentLoggedInOpportunityId(objUse.Id);
           acfCommon.isNullOrEmpty('str');
           acfCommon.IsNullOrEmptyString('strInput');
           acfCommon.getLeadApiNameToAnswerMap('strAnswers');
           acfCommon.getLeadApiNameToType('strAnswers');
           acfCommon.getRequiredDocumentsFromAnswers(objLead.Id,set1);      
           acfCommon.getSuggestedProductsFromAnswers(objLead.Id,set1);
           acfCommon.IsValidDecimal(1);
           acfCommon.calculateMonthlyPayment(1000000.00,12,'Interest Only',12);
           acfCommon.calculateMonthlyPayment(1000000.00,2,'Principal and Interest',1200);
           acfCommon.getCurrentLoggedInLeadId(objUse.Id);
        }
 
     }
     
      private static testMethod void validate1()
    {
        User objUser=new User();
        objUser=acfCommonTrackerClass.CreateUser(objUser);
              
        list<string> lstemail = new list<string>();
        lstemail.add('ankit@gmail.com');
        lstemail.add('ankitsingh@gmail.com');
 
        set<string> set1=new set<String>();
        set1.add('setSelectedAnswer');
        
        Lead objLead = new Lead();
        objLead.acfIs_Post_Login_Ques_Attempted__c = true;
        objLead.acfIs_Identity_Document_Submitted__c = false;
        objLead = acfCommonTrackerClass.createLeadForPortal();
        Account objAccount = new Account();
        objAccount = acfCommonTrackerClass.CreatePersonAccount('test','test','+619540505050','test@fakeemail.com',objLead.id);
        User objUse =new User();
        objUse =  acfCommonTrackerClass.CreatePortalUser(objAccount.id); 
        Opportunity objOpportunity = new Opportunity();
        objOpportunity = acfCommontrackerClass.createOpportunity(objOpportunity,objUse);
    System.runAs(objUse) 
        {
           acfCommon.sendSMS(null,'+619540505050','message',true);
           acfCommon.checkEmail('sushree.biswal@saasfocus.com');
           acfCommon.sendTemplatedEmail(objUser.Id,'strContent','strSubject',lstemail,lstemail);
           acfCommon.SendSMSUsingFuture('recepientName','+619873164685','message',true);
           acfCommon.sendReturnUrl(objUse.Id,false,'pgname'); 
           acfCommon.redirect(objUse.Id,'clickjumiointegration');
           acfCommon.getCurrentLoggedInContactId(objUse.Id); 
           acfCommon.getCurrentLoggedInOpportunityId(objUse.Id);
           acfCommon.isNullOrEmpty('str');
           acfCommon.IsNullOrEmptyString('strInput');
           acfCommon.getLeadApiNameToAnswerMap('strAnswers');
           acfCommon.getLeadApiNameToType('strAnswers');
           acfCommon.getRequiredDocumentsFromAnswers(objLead.Id,set1);      
           acfCommon.getSuggestedProductsFromAnswers(objLead.Id,set1);
           acfCommon.IsValidDecimal(1);
           acfCommon.calculateMonthlyPayment(1000000.00,12,'Interest Only',12);
           acfCommon.calculateMonthlyPayment(1000000.00,2,'Principal and Interest',1200);
           acfCommon.getCurrentLoggedInLeadId(objUse.Id);
        }
 
     }
     
       
      private static testMethod void validate2()
    {
    
        Account objAccou = new Account();
        objAccou = acfCommontrackerClass.createAccount(objAccou);
        
        Script__c objScript = new Script__c();
        Id recorsTypeId  = acfCommon.GetRecordTypeId('Script__c','Post-Login');
        objScript.RecordTypeId = recorsTypeId;
        objScript.acf_Type__c = 'Pre-Login';
        objScript.acf_Sequence_no__c = 1;
        objScript.acf_Question_Type__c = 'Applicant Details';
        insert objScript;
        
        acfBank_Detail__c objbankdetail = new acfBank_Detail__c();
        objbankdetail = acfCommontrackerClass.createBankDetail();
        
        Question__c  objques = new Question__c();
        objques = acfCommontrackerClass.CreatePrePostLoginQuestions('strQuestion',12,'Text',true,true,objScript.id,'test');
        
        Question__c  objqu = new Question__c();
        objqu = acfCommontrackerClass.CreatePostLoginQuestions('strQuestion',12,'Text',true,true,objScript.id,'test','Text',true,'Text',true);
        
        Script__c objScr = new Script__c();
        objScr = acfCommontrackerClass.createScript('Post-Login');
        
        
        
        User objUser=new User();
        objUser=acfCommonTrackerClass.CreateUser(objUser);
              
        list<string> lstemail = new list<string>();
        lstemail.add('ankit@gmail.com');
        lstemail.add('ankitsingh@gmail.com');
 
        set<string> set1=new set<String>();
        set1.add('setSelectedAnswer');
        
        Lead objLead = new Lead();
        objLead.acfIs_Post_Login_Ques_Attempted__c = true;
        objLead = acfCommonTrackerClass.createLeadForPortal();
        
        Lead objl = new Lead();
        objl = acfCommonTrackerClass.createLead(objl);
        
        Contact objContact = new Contact();
        objContact = acfCommonTrackerClass.createContact(objContact,objAccou);
        

         
         MortgageExpert__c objmor = new MortgageExpert__c();
         objmor = acfCommonTrackerClass.createMortgageExpert(objmor);
         
         Product2 objpro = new Product2();
         objpro = acfCommonTrackerClass.createProduct(objpro);
         
         Question__c  objquestio = new Question__c();
         objquestio  = acfCommonTrackerClass.createQuestion(objquestio);
         
         Answer__c objans = new Answer__c();
         objans.acf_Question__c = objquestio.id;
         objans = acfCommonTrackerClass.createAnswer(objans);
         
         acfContentManagementSystem__c objcob = new acfContentManagementSystem__c();
         objcob = acfCommonTrackerClass.CreateContentManagementSystem(objcob);
         
         acfTask_Master__c objtask = new acfTask_Master__c();
         objtask = acfCommonTrackerClass.createtask(objtask);
         
         Answer__c obja = new Answer__c();
         obja = acfCommonTrackerClass.createAnswer(objqu.id,objquestio.id,'test');
         
         acfBank_Detail__c objb = new acfBank_Detail__c();
         objb = acfCommonTrackerClass.createBankDetail(objb);
        
        Profile objjprofile = new Profile();
        objjprofile = acfCommonTrackerClass.createProfile('Community User');
      
        Id objAc = acfCommontrackerClass.CreatePersonAccountNew(objLead.id);
        
        Account objAccount = new Account();
        objAccount = acfCommonTrackerClass.CreatePersonAccount('test','test','+619540505050','test@fakeemail.com',objLead.id);

   //     Id objUs = acfCommonTrackerClass.CreatePortalUserNew(objContact.id);
        User objUse =new User();
        objUse =  acfCommonTrackerClass.CreatePortalUser(objAccount.id);
         
        Opportunity objOpportunity = new Opportunity();
        objOpportunity = acfCommontrackerClass.createOpportunity(objOpportunity,objUse);
    System.runAs(objUse) 
        {
           acfCommon.sendSMS('test',null,'message',true);
           acfCommon.checkEmail('sushree.biswal@saasfocus.com');
           acfCommon.sendTemplatedEmail(objUser.Id,'strContent','strSubject',lstemail,lstemail);
           acfCommon.SendSMSUsingFuture('recepientName','+619873164685','message',true);
           acfCommon.sendReturnUrl(objUse.Id,false,'pgname'); 
           acfCommon.redirect(objUse.Id,'clickjumiointegration');
           acfCommon.getCurrentLoggedInContactId(objUse.Id); 
           acfCommon.getCurrentLoggedInOpportunityId(objUse.Id);
           acfCommon.isNullOrEmpty('str');
           acfCommon.IsNullOrEmptyString('strInput');
           acfCommon.getLeadApiNameToAnswerMap('strAnswers');
           acfCommon.getLeadApiNameToType('strAnswers');
           acfCommon.getRequiredDocumentsFromAnswers(objLead.Id,set1);      
           acfCommon.getSuggestedProductsFromAnswers(objLead.Id,set1);
           acfCommon.IsValidDecimal(1);
           acfCommon.calculateMonthlyPayment(1000000.00,12,'Interest Only',12);
           acfCommon.calculateMonthlyPayment(1000000.00,2,'Principal and Interest',1200);
           acfCommon.getCurrentLoggedInLeadId(objUse.Id);
        }
    }
    
      
      private static testMethod void validate3()
    {
        User objUser=new User();
        objUser=acfCommonTrackerClass.CreateUser(objUser);
              
        list<string> lstemail = new list<string>();
        lstemail.add('ankit@gmail.com');
        lstemail.add('ankitsingh@gmail.com');
 
        set<string> set1=new set<String>();
        set1.add('setSelectedAnswer');
        
        Lead objLead = new Lead();
        objLead = acfCommonTrackerClass.createLeadForPortal();
        Account objAccount = new Account();
        objAccount = acfCommonTrackerClass.CreatePersonAccount('test','test','+619540505050','test@fakeemail.com',objLead.id);
        User objUse =new User();
        objUse =  acfCommonTrackerClass.CreatePortalUser(objAccount.id); 
        Opportunity objOpportunity = new Opportunity();
        objOpportunity = acfCommontrackerClass.createOpportunity(objOpportunity,objUse);
    System.runAs(objUse) 
        {
           acfCommon.sendSMS('test','+619540505050',null,true);
           acfCommon.checkEmail('sushree.biswal@saasfocus.com');
           acfCommon.sendTemplatedEmail(objUser.Id,'strContent','strSubject',lstemail,lstemail);
           acfCommon.SendSMSUsingFuture('recepientName','+619873164685','message',true);
           acfCommon.sendReturnUrl(objUse.Id,false,'pgname'); 
           acfCommon.redirect(objUse.Id,'clickjumiointegration');
           acfCommon.getCurrentLoggedInContactId(objUse.Id); 
           acfCommon.getCurrentLoggedInOpportunityId(objUse.Id);
           acfCommon.isNullOrEmpty('str');
           acfCommon.IsNullOrEmptyString('strInput');
           acfCommon.getLeadApiNameToAnswerMap('strAnswers');
           acfCommon.getLeadApiNameToType('strAnswers');
           acfCommon.getRequiredDocumentsFromAnswers(objLead.Id,set1);      
           acfCommon.getSuggestedProductsFromAnswers(objLead.Id,set1);
           acfCommon.IsValidDecimal(1);
           acfCommon.calculateMonthlyPayment(1000000.00,12,'Interest Only',12);
           acfCommon.calculateMonthlyPayment(1000000.00,2,'Principal and Interest',1200);
           acfCommon.getCurrentLoggedInLeadId(objUse.Id);
        }
    }
}