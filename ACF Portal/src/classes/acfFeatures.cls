public class acfFeatures {

    public list<acfContentManagementSystem__c> lst_sctn_features{get; set;}
    public acfContentManagementSystem__c cms_obj {get; set;}
    
    public acfFeatures() {
        lst_sctn_features = [SELECT acfDescription__c, acfHeading__c, acfImageURL__c, acfMoreDetailURL__c, acfSectionName__c, acfSequence__c,acfDescriptionImageURL__c, Id, Name FROM acfContentManagementSystem__c WHERE acfSectionName__c = 'features' ORDER BY acfSequence__c ASC NULLS FIRST];      
        cms_obj = [select id,name,acfDescription__c,acfPageName__c,acfSectionName__c from acfContentManagementSystem__c  where acfPageName__c = 'common' and acfSectionName__c = 'Footer' limit 1]; 
    }
}