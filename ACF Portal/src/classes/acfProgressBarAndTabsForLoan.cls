public class acfProgressBarAndTabsForLoan {

    public PageReference navToLoan() {
        return page.acfLoanDashboard;
    }
    
    public PageReference navToFeed() {
        return page.acfFeedDashboard;
    }
    
    public PageReference navToTsk() {
        return page.acfTaskDashboard;
    }
    
    public PageReference navToDoc() {
        return page.acfDocumentDashboard;
    }

}