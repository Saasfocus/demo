@isTest
public class acfBankDetailsTest {
        
    public static string userId;
    public static user objUser;
    public static list<acfBank_Detail__c> bank_obj;
    
    public static testMethod void myUnitTest() {    	
        acfBankDetailsTest.insertUser();
        
        System.runAs(objUser) {
	        System.debug('Current User: ' + UserInfo.getUserName());
	        System.debug('Current Profile: ' + UserInfo.getProfileId());
	        
	        PageReference pageRef = Page.acfBankDetails;
	    	Test.setCurrentPage(pageRef);
	    	acfBankDetails bank_details = new acfBankDetails();
	    	
	    	bank_details.bank_api_name = 'bank_of_statements';
	    	bank_details.user = '98765432';
	    	bank_details.password = 'ItsAllGood!';
	    	bank_details.getAllBank();
	    	bank_details.submit(); 
        }
    	
    }
    
    public static testMethod void myUnitTest1() {    	
        acfBankDetailsTest.insertUser();
        
        System.runAs(objUser) {
	        System.debug('Current User: ' + UserInfo.getUserName());
	        System.debug('Current Profile: ' + UserInfo.getProfileId());
	        
	        PageReference pageRef = Page.acfBankDetails;
	    	Test.setCurrentPage(pageRef);
	    	acfBankDetails bank_details = new acfBankDetails();
	    	
	    	bank_details.bank_api_name = 'bank_of_statements';
	    	bank_details.user = '98765432';
	    	bank_details.password = 'ItsAllGood!';
	    	//bankStatementResponse
	    	//acResponse.bankSatementResponseSuccessorFailure bnk_stmt_failure = new acResponse.bankSatementResponseSuccessorFailure(,true,'');
	    	//bnk_stmt_failure.success = true;
	    	//bnk_stmt_failure.message = '';
	    	//bnk_stmt_failure.response = 
        }
    	
    }
    
    public static void insertUser(){
    	bank_obj = new list<acfBank_Detail__c>(); 
    	Schema.DescribeSObjectResult leadRT = Schema.SObjectType.Lead;
        Map<String,Schema.RecordTypeInfo> rtCick_Refi = leadRT.getRecordTypeInfosByName();        
        Schema.RecordTypeInfo rt_Lead =  rtCick_Refi.get('Click Refi');
        account account = new Account(name='ACF Account', acfIsDefaultAccount__c=true);
        insert account;
        
		// ================= Lead ==================
        lead objLead = new Lead();
        objLead.FirstName = 'Test';
        objLead.LastName = 'Test';
        objLead.Email = 'test@gmail.com';
        objLead.RecordTypeId = rt_Lead.getRecordTypeId() ;
        objLead.MobilePhone = '1234567890';
        objLead.Company = 'Saas';
        objLead.Status = 'Open';
        
        AssignmentRule AR = new AssignmentRule();
        AR = [select id from AssignmentRule where SobjectType = 'Lead' and Active = true limit 1];
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.assignmentRuleId = AR.id;
        objLead.setOptions(dmlOpts);
        insert objLead;
        system.debug('objLead--------------- '+objLead);
        
        // ================ Contact ===================
        Contact objContact = new Contact(); 
        objContact.FirstName = objLead.FirstName;
        objContact.LastName = objLead.LastName;
        objContact.Email    = 'test@gmail.com'; 
        objContact.MobilePhone = '1234567890'; 
        objContact.acf_Lead__c = objLead.id;
        objContact.AccountId = Account.id; 
        insert objContact;
        system.debug('objContact--------------- '+objContact);
        
        if(objContact <> null && objContact.id <> null){
            List<Profile> lstCommUserProfile = [select id,name from profile where name='Community User' limit 1];
            system.debug('lstCommUserProfile-----------------------'+lstCommUserProfile);
            //user objUser = new User();
            objUser = new User();
            objUser.FirstName = objLead.firstName;
            objUser.LastName = objLead.LastName;
            objUser.MobilePhone = '1234567890';
            objUser.Username = 'test_bank@gmail.com';
            objUser.Email = 'test@gmail.com'; 
            objUser.ProfileId = lstCommUserProfile[0].id;
            objUser.CommunityNickname = String.valueof(Math.random()+DateTime.now().year()+DateTime.now().month()+DateTime.now().day()+DateTime.now().hour()+DateTime.now().minute()+DateTime.now().second());
            objUser.TimeZoneSidKey = 'Asia/Kolkata';
            objUser.ContactId = objContact.id;
            objUser.Alias = 'standt';
            objUser.EmailEncodingKey='UTF-8';
            objUser.LocaleSidKey='en_US';
            objUser.LanguageLocaleKey='en_US';
            String OTP_PIN = String.valueOf(system.now().getTime()).substring(String.valueOf(system.now().getTime()).length() - 6);            
            system.debug('objUser b4=============== '+objUser); 
            insert objUser;
            system.debug('objUser =============== '+objUser);            
            objLead.acf_Contact__c = objContact.id;
            objLead.acfOneTimePassword__c = OTP_PIN;
            objcontact.acfPassword__c = OTP_PIN;
            update objContact; 
            update objLead;               
        }
        
       acfBank_Detail__c bank_obj1 = new acfBank_Detail__c(acfBank_Name__c='Bank of Statements',acfBankSlugName__c='bank_of_statements',acfUserAPI__c=true);
       bank_obj.add(bank_obj1);
       acfBank_Detail__c bank_obj2 = new acfBank_Detail__c(acfBank_Name__c='Bank of Statements2',acfBankSlugName__c='bank_of_statements1',acfUserAPI__c=true);
       bank_obj.add(bank_obj2);
       insert bank_obj;
       system.debug('bank_obj-------------------'+bank_obj);
       
    }
}