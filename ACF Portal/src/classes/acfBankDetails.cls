public class acfBankDetails {

    public string user {get; set;}
    public string password {get; set;}
    public string bank_api_name {get; set;}
    public string msg {get; set;}
    public string str_msg {get; set;}
    public string success_strmsg {get; set;}
    
    public boolean account_details {get; set;}
    public boolean account_data {get; set;}
    public boolean success_msg {get; set;}
    
    public user user_obj;
    public lead lead_obj;
    public contact con_obj;
    
    public acfBank_Detail__c select_bank_name;
    //public list<acfBank_Detail__c> bank_obj {get; set;}
    public list<string> all_bank_obj {get; set;}
    public list<acfBank_Detail__c> bank_list;
    public list<AllAccountsClass> all_account_data {get; set;}
    
    
    
    /*
    *
    * =========== Constructor Method ==============
    */
    public acfBankDetails(){
        
        user = 'Enter your user name';
        password = 'Password';
        
        msg = '';
        str_msg = '';
        success_strmsg = '';        //success_strmsg = 'acfStatementSendSuccessfully';
        
        account_details = true;
        account_data = false;
        success_msg = false;
        
        all_account_data = new list<AllAccountsClass>();         
        
        id user_id = userInfo.getUserId();
        
        //bank_obj = [select id,name,acfAttachmentId__c,acfBank_Logo__c,acfBankSlugName__c,acfBank_Name__c,acfImage_URL__c,acfUserAPI__c from acfBank_Detail__c where acfUserAPI__c =: true];
        //system.debug('bank_obj =========== '+bank_obj);
        
        bank_list = [select id,name,acfBank_Name__c,acfBankSlugName__c from acfBank_Detail__c];    //where acfBank_Name__c =: lead_obj.Current_Lender__c
        system.debug('bank_lst=========== '+bank_list);
            
        user_obj = [select id,name,ContactId from user where id =: user_id];
        system.debug('user_obj =========== '+user_obj );
              
        if(user_obj.ContactId != null){
            con_obj = [select id,acf_Lead__c from contact where Id =: user_obj.ContactId]; 
            system.debug('con_obj =========== '+con_obj);
              
            lead_obj = [select id,name,acfBankNameForAccountDetails__c,ACF_BankName__c,Current_Lender__c from lead where id =: con_obj.acf_Lead__c];
            system.debug('lead_obj =========== '+lead_obj);            
        }
    }
    
    Public pagereference redirect(){
        String oppId = acfCommon.getCurrentLoggedInOpportunityId(userInfo.getuserId()); 
        If(oppId != null && oppId <> ''){
           return new Pagereference('/acfLoanDashboard');
        }else{
                return null;
             }
    }
    
    /*
    * 
    * this method is responsible for show all the bank name in picklist on page
    */
    public List<SelectOption> getAllBank() {
        List<SelectOption> opt = new List<SelectOption>();
        opt.add(new SelectOption('', 'none'));
        if(bank_list != null){
            for(acfBank_Detail__c bnk_data : bank_list){  
                opt.add(new SelectOption(bnk_data.acfBankSlugName__c,bnk_data.acfBank_Name__c));
            }            
        }
        return opt;    
    }
    
    
    /*
    * 
    * this method is responsible for getting all the account details based on there user name and password
    */
    public PageReference submit() {
        system.debug('bank_api_name======== '+bank_api_name+' user ======= '+user+' password ========= '+password);
        msg = '';
        str_msg = '';
        success_strmsg = '';
        if(bank_api_name != '' && bank_api_name != null){
            if(user != null && user != '' && user != 'Enter you user name'){
                if(password != null && password != '' && password != 'Password'){
                    acfbankStatementRequest bank_stmt_req = new acfbankStatementRequest ();
                    acResponse.bankSatementResponseSuccessorFailure bnk_stmt_success_failure = bank_stmt_req.loginRequest(bank_api_name, user, password, true);     
                    system.debug('bnk_stmt_success_failure ================='+bnk_stmt_success_failure );
                    
                    if(bnk_stmt_success_failure.success == true){
                        
                        bankStatementResponse.cls_accounts bnk_stmt_cls_account = new bankStatementResponse.cls_accounts();
                        bnk_stmt_cls_account = bnk_stmt_success_failure.response.accounts;
                        system.debug('bnk_stmt_cls_account true==========='+bnk_stmt_cls_account);
                        if(all_account_data != null){
                            all_account_data.clear();
                        }
                        
                        for(bankStatementResponse.cls_accounts bnk_cls_account : bnk_stmt_cls_account.accounts){
                            
                            AllAccountsClass all_account = new AllAccountsClass();
                            all_account.accountHolder = bnk_cls_account.accountHolder;
                            all_account.name = bnk_cls_account.name;
                            all_account.id = bnk_cls_account.id;
                            all_account.available = bnk_cls_account.available;
                            all_account.balance = bnk_cls_account.balance;
                            all_account.bsb = bnk_cls_account.bsb;
                            all_account.accountNumber = bnk_cls_account.accountNumber;
                            all_account.chk_box = false;
                            
                            all_account_data.add(all_account); 
                        }
                        
                        //code added by prashant on 12/16/2014
                        /*nn
                        acfFetchBankStatements objacfBankStatement =  new acfFetchBankStatements();
                        
                        if(password != null && password != '' && user != null && user != '' && bank_api_name != null && bank_api_name != '')
                        {
                            objacfBankStatement.FetchUserToken(bank_api_name, user, password, '1', con_obj.acf_Lead__c);    
                            account_details = false;
                        }
                        
                        msg = 'acfSelectAaccount';
                        acfBank_Detail__c bnkObj = [select id,name,acfBank_Name__c,acfBankSlugName__c from acfBank_Detail__c where acfBankSlugName__c =: bank_api_name];        
                        lead leadObj = new lead(acfBankNameForAccountDetails__c=bnkObj.id,acfIs_Bank_Statement_Submitted__c=true, id=con_obj.acf_Lead__c);
                        update leadObj;
                    
                        account_details = false;
                        account_data = true;
                        */
                        
                        account_details = false;
                        account_data = true;
                        
                    }else{
                        system.debug('bnk_stmt_success_failure false=========='+bnk_stmt_success_failure);
                        account_details = true;
                        account_data = false;
                        user = 'Enter you user name';
                        password = 'Password';
                        bank_api_name = '';
                        str_msg = 'acfbankdetailLoginErrMsg';
                    }
                }else{
                    str_msg = 'acfProvidePpassword';
                }
            }else{
                str_msg = 'acfProvideUserNname';
            }                   
        }else{
            str_msg = 'acfSelectBankName';  
        } 
        return null;
    }
    
    
    /*
    * 
    * This method is responsible for adding attachment on contact id based on selected account
    */
    public PageReference selectedAccounts(){
        
        str_msg = '';
        success_strmsg = '';
        list<AllAccountsClass> selected_account_list = new list<AllAccountsClass>();
        string account_num = '';
        
        for(AllAccountsClass allAcc : all_account_data){
            if(allAcc.chk_box == true){
                selected_account_list.add(allAcc);
                account_num = allAcc.accountNumber + ',' + account_num;
            }        
        }
        
        account_num = account_num.removeEnd(',');
        
        if(selected_account_list.size() > 0){
            try{
                
                
               acfbankStatementRequest bank_stmt_req = new acfbankStatementRequest ();
                acResponse.bankSatementResponseSuccessorFailure bnk_stmt_success_failure = bank_stmt_req.loginRequest(bank_api_name, user, password, true);     
                system.debug('bnk_stmt_success_failure ================='+bnk_stmt_success_failure );
                    
                acfFetchBankStatements objacfBankStatement =  new acfFetchBankStatements();
                        
                if(password != null && password != '' && user != null && user != '' && bank_api_name != null && bank_api_name != '')
                {
                    objacfBankStatement.FetchUserToken(bank_api_name, user, password, '1', con_obj.acf_Lead__c);    
                    account_details = false;
                }
                
                 /*// Commented by Neeraj. 11 Dec 
                Attachment attachment = new attachment();
                pagereference pageref = new pagereference('/partners/acf_insertPdfBAnkStatement?password=TestMyMoney&username=12345678&BankSlugname=bank_of_statements&Accounts='+account_num);
                
                system.debug('NRAC:::::2::'+password+'::'+user+'::'+bank_api_name);
                
                pagereference pageref = new pagereference('/partners/acf_insertPdfBAnkStatement?password='+password+'&username='+user+'&BankSlugname='+bank_api_name+'&Accounts='+account_num);
                
                attachment.Body = pageref.getContent();     //bank_api_name, user, password
                attachment.Name = 'BankStatement.pdf';
                attachment.ParentId = con_obj.acf_Lead__c;          //user_obj.ContactId;        //current loged in user contact id 
                insert attachment;
                */
                success_msg = true;
                account_data = false;
                success_strmsg = 'acfStatementSendSuccessfully';
                
            }catch(DMLException e){
                system.debug('exception ================'+e);
            }
        }else{
            msg = '';
            str_msg = 'acfSelectAaccount';
        }    
        return null;
    }
    
     public acResponse.bankSatementResponseSuccessorFailure loginRequest(String bank_Slug_Nmme,String username,String password,Boolean istest)
    {
                acResponse.bankSatementResponseSuccessorFailure return_Ref = new acResponse.bankSatementResponseSuccessorFailure(Null,false,Null);
                
                String response;
                if(String.isBlank(String.valueof(bank_Slug_Nmme)))
                {
                response = 'Bank Slug Name is Mandatory.';
                
                return new acResponse.bankSatementResponseSuccessorFailure(Null,false,response);
                }
                if(String.isBlank(String.valueof(username)))
                {
                response = 'User Name is Mandatory.';
                return new acResponse.bankSatementResponseSuccessorFailure(Null,false,response);
                }
                if(String.isBlank(String.valueof(password)))
                {
                response = 'Password is Mandatory.';
                return new acResponse.bankSatementResponseSuccessorFailure(Null,false,response);
                }
                                String body = '{"credentials": {"institution": "'+bank_Slug_Nmme+'","username": "'+username+'","password": "'+password+'"}}';
                                HttpRequest req = new HttpRequest();
                                req.setEndpoint('https://test.bankstatements.com.au/api/v1/login');
                                req.setMethod('POST');
                                req.setHeader('content-type', 'application/json');
                                req.setHeader('Accept-Language', 'en-US,en;q=0.8');
                                req.setHeader('Accept-Encoding', 'gzip,deflate,sdch');
                                req.setHeader('Accept', '*/*');
                                req.setHeader('X-API-KEY', 'QD6TMQI6MLXHKNV6SLILNZECRGOWFWVUOLELF4HF');
                                req.setbody(body);
                                Http http = new Http();
                                String Statuscode;
                                String responseFromHttp;
                                if(isTest == true)
                                {
                                                try
                                                {
                                                HTTPResponse res = http.send(req);
                                                 Statuscode = String.valueOf(res.getStatusCode());
                                                 responseFromHttp = res.getBody();
                                                 System.debug('@@@@@ - '+responseFromHttp);
                                                 if(Statuscode == '200')
                                                 {
                                                        return new acResponse.bankSatementResponseSuccessorFailure(bankStatementResponse.parse(responseFromHttp),true,'Successful Hit.');
                                                 }else
                                                 {
                                                        return new acResponse.bankSatementResponseSuccessorFailure(Null,false,'Unsuccessful Hit. Response from Server is : '+response);
                                                 }
                                                 
                                                }catch(Exception e)
                                                {
                                                        response = e.getMessage();
                                                        return new acResponse.bankSatementResponseSuccessorFailure(Null,false,response);
                                                }
                                }else
                                {
                                        Statuscode = '200';
                                        response = 'Test Mode';
                                        return new acResponse.bankSatementResponseSuccessorFailure(Null,true,response);
                                }
                                
                
                return return_Ref;
                
        }
    
    
    /*
    * 
    *  This method is responsible for adding attachment on contact id based on selected account
    */
    public PageReference OK(){
        success_msg = false;
        String retUrl = acfcommon.sendReturnUrl(user_obj.id,false,'acfBankDetails');
        PageReference pageref = new PageReference('/partners/'+retUrl);
        return(pageref);
       // return page.acfResetPassword;
    }
    
    /*
    *
    *  This is the Wrapper class for binding the account details on VF Page
    */
    public class AllAccountsClass{
        public boolean chk_box {get; set;}
        public String accountHolder {get; set;}
        public String name {get; set;} 
        public String accountNumber {get; set;} 
        public String id {get; set;} 
        public String bsb {get; set;} 
        public String balance {get; set;}  
        public String available {get; set;}  
    }
    
    /*
    *
    *  This is the Wrapper class for binding the ERROR message
    */
    public class ErrorMessage{
        public string error {set; get;} 
        public integer errorcode {set; get;}
        public string user_token {set; get;}
    }
}