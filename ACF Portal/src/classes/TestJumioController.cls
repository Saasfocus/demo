public class TestJumioController 
{
   transient Attachment attach;
   transient Attachment docattach;
   public blob DocumnetBlob; 
   public TestJumioController ()
   {
       attach = new Attachment ();
   }
   
   public Attachment getattach() 
   {
    if(attach == null)
        attach = new Attachment();
     return attach ;
   }
   public Attachment getdocattach()
   {
    if(docattach== null)
        docattach = new Attachment();
     return docattach;
   }
   public pageReference UploadDoc2()
   {
     DocumnetBlob = attach.body;
     return null;
   }
   public pageReference UploadDoc1()
   {
    DocumnetBlob = attach.body;
     system.debug('@$#$%%'+DocumnetBlob);
     string jsonRequest;
     jsonRequest = '{"merchantIdScanReference":"1234566ABCDEDFRT","frontsideImage":"'+EncodingUtil.base64Encode(DocumnetBlob)+'","faceImage":"'+EncodingUtil.base64Encode(docattach.body)+'"}';
     UploadJumioDocument(jsonRequest);
     Attachment attach = new Attachment();
     return null;
   }
   public pageReference UploadJumioDocument(string jsonRequest)
   {
         //Create Http request
        Jumio_Credentials__c objJumioCredential = Jumio_Credentials__c.getValues('Credential');       
        
        String strUserName = objJumioCredential.Merchant_API_token__c;
        String strPassword = objJumioCredential.Active_API_secret__c;    
        String strEndPoint = objJumioCredential.EndPoint__c;
        String strAuthorizationHeader = 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(strUserName + ':' + strPassword)); 
        
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint(strEndPoint);
        System.debug('!!!! strAuthorizationHeader -'+ strAuthorizationHeader);
        req.setHeader('Authorization', strAuthorizationHeader);
        req.setHeader('Accept', 'application/json');             
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('User-Agent', 'SaasFocus Saas/1.9.0');
        req.setBody(jsonRequest);
        
        //send request        
        Http http = new Http();
        HTTPResponse jsonResponse = http.send(req);
        String jsonResponseBody = jsonResponse.getBody(); 
        system.debug('@@@json'+jsonResponseBody);  
       
       return null;
   }
}