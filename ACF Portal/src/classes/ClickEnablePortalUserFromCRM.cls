global class ClickEnablePortalUserFromCRM 
{ 
   webservice static string createPortalUserFromCRM(string strLeadFName,string strLeadLName,string strLeadEmail,string strPhone,string strLeadId)
   {
   	  if(IsValidUserName(strLeadEmail))
   	  {
       //Creating Person Account
        Account objPersonAccount      = new Account();
        objPersonAccount.FirstName    = strLeadFName; 
        objPersonAccount.LastName     = strLeadLName;
        objPersonAccount.PersonMobilePhone        = strPhone;
        objPersonAccount.PersonEmail  = strLeadEmail;
        objPersonAccount.acf_Lead__pc = strLeadId;
        insert objPersonAccount;
        //List To get Person Account Contact.
        List<Account>lstPersonContact = [select id,PersonContactId from Account where id=:objPersonAccount.Id];
        if(lstPersonContact <> null && lstPersonContact.size()>0)
        {
            List<Profile> lstCommUserProfile = [select id,name from profile where name='Community User' limit 1];
            User objUser = new User();
            objUser.FirstName = strLeadFName;
            objUser.LastName = strLeadLName;
            objUser.Alias  = (strLeadLName+String.valueof(Math.random())).substring(0,5);
            objUser.LocaleSidKey = 'en_AU';
            objUser.LanguageLocaleKey = 'en_US';
            objUser.MobilePhone = strPhone;
            objUser.Username = strLeadEmail;
            objUser.Email = strLeadEmail;
            objUser.ProfileId = lstCommUserProfile[0].id;
            objUser.ContactId = lstPersonContact[0].PersonContactId;
            objuser.EmailEncodingKey = 'UTF-8';
            objUser.CommunityNickname = String.valueof(Math.random()+DateTime.now().year()+DateTime.now().month()+DateTime.now().day()+DateTime.now().hour()+DateTime.now().minute()+DateTime.now().second());
            objUser.TimeZoneSidKey = 'Australia/Brisbane';
            objUser.acfIs_Password_Reset__c = true;
            Database.DMLOptions dlo = new Database.DMLOptions();
			dlo.EmailHeader.triggerUserEmail = false;
			objUser.setOptions(dlo);
			insert(objUser);
			//system.setPassword(objUser.id, 'testpassword1');
            //Updating Account and Contact in Lead.
            Lead objLead = new Lead(id=strLeadId);
            objLead.acf_Contact__c = lstPersonContact[0].PersonContactId;
            objLead.acf_partner_account__c = objPersonAccount.id;
            update objLead;
            acfCustomerRegisterController.generateReqDocs(strLeadId);
            return 'Portal user Created.';
        }
   	  }
   	  else
   	  {
   	     return 'Email already Exist, Please use different one.';	
   	  }  
      return '';   
   }
   public static boolean IsValidUserName(string struseEmail)
   {
   	    List<User>lstUser = [select id,Name from user where UserName=:struseEmail];
   	    if(lstUser <> null && lstUser.size()>0)
   	    {
   	    	return false;
   	    }
   	    return true;
   }
}