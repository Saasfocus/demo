/*
    This class is used to update the success checkbox on lead
    =========================================================
    Name                     Date                    Version
    =========================================================
    Prashant               11/18/2014                    1.0
    ========================================================= 
*/
public class acfJumioSuccess 
{
    Public String retUrlPage {get;set;}
    //Constructor
    public void acfJumioSuccess(){   
    }
    
    /*
        This method is used to update the success checkbox on lead
        =========================================================
        Name                     Date                    Version
        =========================================================
        Prashant               11/18/2014                    1.0
        ========================================================= 
    */
    public void UpdateSuccessOnLead()
    {
        string strUserId = UserInfo.getUserId();
        if(strUserId != null && strUserId != '')
        {
            list<User> lstUser = [select id, ContactId from User where Id =: strUserId and IsActive = true];
            String oppId = acfCommon.getCurrentLoggedInOpportunityId(userInfo.getuserId());
            If(oppId != null && oppId <> ''){
               List<Opportunity> lstOpportunity = [select id,acfIs_Identity_Document_Submitted__c from Opportunity where id =: oppId];
               If(lstOpportunity != null && lstOpportunity.size()>0){
                   lstOpportunity[0].acfIs_Identity_Document_Submitted__c = true;
                   lstOpportunity[0].clickJumio_Status__c = 'Attempted';
                   Update lstOpportunity[0];
               }
            }else{
                    if(lstUser != null && lstUser.size() > 0 && lstUser[0].ContactId != null)
                    {
                        list<Lead> lstLead = [select id , acf_Contact__c,acfIs_Identity_Document_Submitted__c,(select id,acfStatus__c from Required_Documents__r where acf_Is_Identity_Verification_Doc__c = true limit 1) from Lead where acf_Contact__c =: lstUser[0].ContactId];
                        
                        if(lstLead != null && lstLead.size() > 0)
                        {
                           lstLead[0].acfIs_Identity_Document_Submitted__c = true;
                           lstLead[0].clickJumio_Status__c = 'Attempted';
                           if(lstLead[0].Required_Documents__r <> null && lstLead[0].Required_Documents__r.size()>0)
                           {
                           	  lstLead[0].Required_Documents__r[0].acfStatus__c = 'Awaited';
                           	  update lstLead[0].Required_Documents__r[0];
                           }
                           Update lstLead[0];
                           list<task> lstTask = [select id,subject,status,acf_Is_Upload_Identity_Verification__c from task where acf_Is_Upload_Identity_Verification__c = true limit 1];
                           if(lstTask != null && lstTask.size()>0)
                           {
                           	lstTask[0].status = 'Completed';
                           	update lstTask[0];
                           }
                           system.debug('lstLead[0].id -------------------------'+lstLead[0].id);
                           retUrlPage =  acfcommon.sendReturnUrl(strUserId,false,'acfJumioSuccess');
                           system.debug('pname@@@@'+retUrlPage);
                           /*acfJumioBatchClass Jumio_batch = new acfJumioBatchClass(lstLead[0].id);
                           dataBase.executeBatch(Jumio_batch);
                           system.debug('ending----------------------------');*/
                        }
                    } 
                 }
        }
    }
}