Public class acfAccountDetailController 
	
{
    public String strHeaderName {get;set;}
    public String strDescriptionName {get;set;}    
    Public acfAccountDetailController()
    {
        List<User> lstUser = new List<User>();
        String strDashBoardName = '';
        string pge = Apexpages.currentPage().getUrl();
        List<String> parts = pge.split('/'); 
        List<String> parts1 = parts[2].split('\\?');
        String strPagename = parts1[0]; 
        strHeaderName = '';
        strDescriptionName = '';
        system.debug('current strPagename -------- '+strPagename);
        string strUserId = UserInfo.getuserId();
        If(strUserId != null && strUserId <> '')
        {
         lstUser = [select id,Name from user where id=:strUserId];
        }
        List<acfContentManagementSystem__c> lstDashboardSec = [SELECT Id,acfDescription__c,acfHeading__c, acfSectionName__c FROM acfContentManagementSystem__c where acfSectionName__c = 'Dashboard' limit 1];
		If(lstDashboardSec != null && lstDashboardSec.size()>0)
		{
			acfContentManagementSystem__c ObjCms = lstDashboardSec[0];
			if(lstUser != null && lstUser.size()>0)
			{
				strHeaderName = ObjCms.acfHeading__c.replace('<User Name>',lstUser[0].Name);
			}
			if(strPagename == 'acfLoanDashboard')
			{
				strDashBoardName = 'Loan';
			} 
			else if(strPagename == 'acfFeedDashboard')
			{
				strDashBoardName = 'Feed';
			}
			else if(strPagename == 'acfTaskDashboard')
			{
				strDashBoardName = 'Task';
			}
			else if(strPagename == 'acfDocumentDashboard')
			{
				strDashBoardName = 'Document';
			}
			else if(strPagename == 'acfBookAppointment')
			{
				strDashBoardName = 'Book Appointment';
			}
			else if(strPagename == 'acfViewDetails')
			{
				strDashBoardName = 'View Detail';
			}	
			strDescriptionName = ObjCms.acfDescription__c.replace('<Dashboard Name>',strDashBoardName);			
		}
    }       
}