public class acfProgressBarAndTabs 
{
    public string pge_name {set; get;}
    Public List<Task> List_Task;
    Public Integer CountTasks {get;set;} 
    Public Boolean isApplication {get;set;}
    Public Boolean isApproval {get;set;}
    Public Boolean isLoanDocs {get;set;}
    Public Boolean isSettlement {get;set;} 
    Public Boolean isDocumentRejected {get;set;}
    public acfProgressBarAndTabs()
    { 
    	string pge = Apexpages.currentPage().getUrl();
    	Id loggedInLeadId = acfCommon.getCurrentLoggedInLeadId(userinfo.getUserId());
    	Id loggedInOpportunityId = acfCommon.getCurrentLoggedInOpportunityId(userinfo.getUserId());
        List<String> parts = pge.split('/'); 
        List<String> parts1 = parts[2].split('\\?');
        pge_name = parts1[0]; 
        system.debug('cons pge_name -------- '+pge_name);  
        isApplication = false;
        isApproval = false;
        isLoanDocs = false;
        isSettlement = false;       
        if(loggedInOpportunityId != null)
        {  
               Opportunity oppObj = [select id,acf_Status__c,acfIs_Identity_Document_Rejected__c,StageName from opportunity where Id=:loggedInOpportunityId];
                If(oppObj!=null)
                {
                	isDocumentRejected = oppObj.acfIs_Identity_Document_Rejected__c;
                    If(oppObj.StageName == 'Application Taken')
                    {
                        isApplication = true;
                    }
                    else If(oppObj.StageName == 'Approval')
                    {
                        isApproval = true;
                    }
                    else If(oppObj.StageName == 'Loan Document')
                    {
                        isLoanDocs = true;
                    } 
                    else If(oppObj.StageName == 'Settlement' || oppObj.StageName == 'Settled')
                    {
                       isSettlement = true;
                    }
                }
                List_Task = new List<Task>();
                List_Task = [SELECT ActivityDate, Id, Status,IsVisibleInSelfService, Subject, WhatId, WhoId, who.name FROM Task WHERE WhatId =: oppObj.Id AND Status != 'Completed'];
                If(List_Task!=null && !List_Task.isEmpty())
                {
                    CountTasks = List_Task.size();
                }
                else
                {
                    CountTasks = 0;
                }
            }else If(loggedInLeadId != null){
            	Lead objLead = [select id,acfIs_Identity_Document_Rejected__c from lead where id=:loggedInLeadId];
            	If(objLead != null){
            		isDocumentRejected = objLead.acfIs_Identity_Document_Rejected__c;
            	}
            }
        }
    }