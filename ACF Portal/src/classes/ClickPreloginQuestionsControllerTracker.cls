@isTest(SeeAllData=true)
private class ClickPreloginQuestionsControllerTracker {
private static Question__c objQuestion;
private static Script__c objScript;
static private User objUser;
private static acfBank_Detail__c objBankDetail;
private static Answer__c objAnswer;
private static list<Answer__c> lstAnswer;

    static testMethod void validatepreLogin()
    {
        
        objScript = new Script__c();
        objScript.acf_Type__c = 'Pre-Login';
        objScript.acf_Sequence_no__c = 1;
        insert objScript;
        
        Question__c objQuest = new Question__c ();
        objQuest.acf_Question__c = 'Who is your current lender?';
        objQuest.acf_Active__c = true;
        objQuest.acf_Sequence_No__c = 2.0;
        objQuest.acf_Type__c = 'Button';
        objQuest.acfRefinance_Sequence_No__c = 1;
        objQuest.acfIs_refinance__c = true;
        objQuest.acf_Script__c = objScript.id;
        objQuest.acfIs_New_Loan__c = true;
        insert objQuest;
        LoadData();
        system.debug('@@##'+objScript);
        Cookie IsNewHomeLoan = new Cookie('IsNewHomeLoan','true','true',72000, true); 
        
        ApexPages.currentPage().setCookies( new Cookie[]{IsNewHomeLoan});
        Cookie Bname = new Cookie('Bname','Westpac Bank',null,72000,true); 
        ApexPages.currentPage().setCookies( new Cookie[]{Bname});
        Cookie ACFQues = new Cookie('ACFQues',objQuest.id + '-:-' +'test'+'-,-',null,72000, false); 
        ApexPages.currentPage().setCookies( new Cookie[]{ACFQues}); 
        map<string,string>mapCookiesQues = new map<string,string>();
        ClickPreloginQuestionsController.WrapperQuestions objwap = new ClickPreloginQuestionsController.WrapperQuestions();
        mapCookiesQues.put(objQuest.id,Bname.getvalue());    
        ClickPreloginQuestionsController objacfpreloginQuestionController = new ClickPreloginQuestionsController();
        objacfpreloginQuestionController.getMapQuesIDToSelectedAnswer('test-:-ankit-:-test-:-ankit');
        objacfpreloginQuestionController.RelatedQuestions();
        objacfpreloginQuestionController.AddingRelatedQuestion();
        objacfpreloginQuestionController.NextQuestion();
        objacfpreloginQuestionController.previousQuestion();
        ClickPreloginQuestionsController.AutoCompleteBanklList('Bank of Statements'); 
        
        
    }
    
    static testMethod void validatepreLogin1()
    {
        
        objScript = new Script__c();
        objScript.acf_Type__c = 'Pre-Login';
        objScript.acf_Sequence_no__c = 1;
        insert objScript;
        
        Question__c objQuest = new Question__c ();
        objQuest.acf_Question__c = 'Who is your current lender?';
        objQuest.acf_Active__c = true;
        objQuest.acf_Sequence_No__c = 2.0;
        objQuest.acf_Type__c = 'Button';
        objQuest.acfRefinance_Sequence_No__c = 1;
        objQuest.acfIs_refinance__c = true;
        objQuest.acf_Script__c = objScript.id;
        objQuest.acfIs_New_Loan__c = true;
        insert objQuest;
        LoadData();
        system.debug('@@##'+objScript);
        Cookie IsNewHomeLoan = new Cookie('IsNewHomeLoan',objQuest.id + '-:-' +'test'+'-,-',null,72000, false); 
        ApexPages.currentPage().setCookies( new Cookie[]{IsNewHomeLoan});
        Cookie Bname = new Cookie('Bname','Westpac Bank',null,72000,true); 
        ApexPages.currentPage().setCookies( new Cookie[]{Bname});
        Cookie ACFQues = new Cookie('ACFQues',null,null,72000, false); 
        ApexPages.currentPage().setCookies( new Cookie[]{ACFQues}); 
        map<string,string>mapCookiesQues = new map<string,string>();
        mapCookiesQues.put(objQuest.id,Bname.getvalue()); 
        
        ClickPreloginQuestionsController.WrapperQuestions objwap = new ClickPreloginQuestionsController.WrapperQuestions();  
          
        ClickPreloginQuestionsController objacfpreloginQuestionController = new ClickPreloginQuestionsController();
        objacfpreloginQuestionController.isNavigated = true;
        objacfpreloginQuestionController.RelatedQuestions();
        objacfpreloginQuestionController.AddingRelatedQuestion();
        objacfpreloginQuestionController.NextQuestion();
        objacfpreloginQuestionController.previousQuestion();
        objacfpreloginQuestionController.clearCookies();
        objacfpreloginQuestionController.resumePreviousSession();
        objacfpreloginQuestionController.IsValidId('test');
        objacfpreloginQuestionController.getLeadApiNameToAnswerMap('test');
        ClickPreloginQuestionsController.AutoCompleteBanklList('Bank of Statements'); 

        
    }
    
    private static void LoadData()
    {
        createScript();
        createQuestion();
   //     createAnswer();
        createBankDetail();
        
    }
    
    static void createScript()
    {
      //  objScript = new Script__c();
      //  objScript = acfCommonTrackerClass.createScript(objScript);
        Script__c objscr = new Script__c();
        objscr.acf_Sequence_no__c = 1;
        objscr.acf_Type__c= 'Pre-Login'; 
        insert objscr ;
        
        
        Question__c objQues = new Question__c ();
        objQues.acf_Question__c = 'Who is your current lender?';
        objQues.acf_Active__c = true;
        objQues.acf_Sequence_No__c = 2.0;
        objQues.acf_Type__c = 'Button';
        objQues.acfRefinance_Sequence_No__c = 1;
        objQues.acfIs_refinance__c = true;
        objQues.acf_Script__c = objscr.id;
        insert objQues;
    }
    
    static void createQuestion()
    {
      //  objQuestion = new Question__c();
        //objQuestion.acf_Master_Question__c = objQuestion.id;
        //objQuestion.acf_Script__c = objScript.id;
     //   objQuestion= acfCommonTrackerClass.createQuestion(objQuestion,objScript);
        
    }
    
    static void createAnswer()
    {
        objAnswer = new Answer__c();
        lstAnswer = new list<Answer__c>();
        objAnswer.acf_Question__c = objQuestion.id;
        objAnswer.acf_Related_Question__c = objQuestion.id;
        objAnswer.acf_Answer__c = 'test';
        objAnswer.acfSequence_No__c = 1;
        //objAnswer = acfCommonTrackerClass.createAnswer(objAnswer);
        lstAnswer.add(objAnswer);
        insert lstAnswer;
    }
    
    static void createBankDetail()
    {
        objBankDetail = new acfBank_Detail__c();
        objBankDetail = acfCommonTrackerClass.createBankDetail(objBankDetail);
    }
    
    
}