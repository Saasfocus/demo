@isTest
                        
global class BatchMockHttpResponseGenerators implements HttpCalloutMock {
    
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        System.assertEquals('https://netverify.com/api/netverify/v2/scans/S001', req.getEndpoint());
        System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"timestamp": "2012-08-16T10:27:29.494Z","authorizationToken": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx","clientRedirectUrl": "https://[your-domain-prefix].netverify.com/v2?authorizationToken=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx","jumioIdScanReference": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"}');
        res.setStatusCode(201);
        return res;
    }
}