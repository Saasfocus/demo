Public class acfId_VerifyController {
    Public String retUrl;
    Public List<User> lstuser;
    Public Boolean isSkipvisible {get;set;}
    //Public String UserId;
    Public acfId_VerifyController()
    {
         lstuser = [select Id, ContactId, Contact.acf_Lead__c from User where Id =: userinfo.getuserId()];
         isSkipvisible = true;
        //UserId = userinfo.getuserId();
        String strLeadId = acfCommon.getCurrentLoggedInLeadId(userinfo.getuserId());
        If(strLeadId <> '' && strLeadId !=null)
        {
            List<Lead> lstLead = [select id,acfIs_Bank_Statement_Submitted__c,acfIs_Post_Login_Ques_Attempted__c from lead where id=:strLeadId];
            If(lstLead != null && lstLead.size()>0)
            {
                   If(lstLead[0].acfIs_Bank_Statement_Submitted__c && lstLead[0].acfIs_Post_Login_Ques_Attempted__c)
                   {
                        isSkipvisible = false;
                   }
            }
        }
        retUrl = '';  
        //ClickQuoteDetail.intWidth = 10;
        ClickQuoteDetail.setProgressBarWidth(0,0,'clickid_verification');
    }
    
    Public pagereference redirectToJumio()
    {
        Pagereference pageref = page.Clickjumiointegration;
        pageref.setredirect(true);
        return pageref;
    }
    
    Public pagereference redirectToNextPage()
    {
        string loggedInOppId = acfCommon.getCurrentLoggedInOpportunityId(UserInfo.getuserId());
        if(loggedInOppId == null)
        {
            string strLoggedInleadid = acfcommon.getCurrentLoggedInLeadId(userInfo.getuserId());
            lead leadObj = new lead(acfIs_Identity_Document_Submitted__c =true,clickJumio_Status__c = 'Skipped', id= strLoggedInleadid);
            update leadObj;
        }else{
            opportunity objOpp = new opportunity(acfIs_Identity_Document_Submitted__c =true,clickJumio_Status__c = 'Skipped',id= loggedInOppId);
            update objOpp;
        }
        String retPgname = acfCommon.sendReturnUrl(userinfo.getuserId(),false,'clickid_verification');
        Pagereference pageref = new Pagereference('/partners/'+retPgname);
        //Pagereference pageref = page.acfBankDetails;
       // pageref.setredirect(true);
        return pageref;
    }
    
    Public pagereference redirect()
    {
      string returnURL = acfcommon.redirect(userinfo.getuserId(),'clickid_verification');
      if(returnURL != null && returnURL <> '' && returnURL != 'false')
      {
           return new Pagereference('/'+returnURL);
      }else{
          return null;
      }
      /*  String strOppId = acfCommon.getCurrentLoggedInOpportunityId(userInfo.getuserId());
        If(strOppId != null && strOppId <> ''){
          List<Opportunity> lstOpportunity = [select id,acfIs_Identity_Document_Rejected__c,acfIs_Identity_Document_Submitted__c from Opportunity where id =: strOppId];
            If(lstOpportunity != null && lstOpportunity.size()>0){ 
              If(lstOpportunity[0].acfIs_Identity_Document_Rejected__c == false)       
               return new Pagereference('/clickdashboard');
          }
        }
       return null;   */     
    }
}