@isTest(SeeAllData = false)
private class acfBankDetailsTracker {
	private static User objUser;
	private static Contact objContact;
	private static Lead objLead;
	private static Account objAccount;
	private static Opportunity objOpportunity;
	private static acfBank_Detail__c objBankDetail;

    static testMethod void validateacfBankDetails() 
    {
        LoadData();
		acfBankDetails objacfBankDetails = new acfBankDetails();
		objacfBankDetails.user = 'swati.sharma@saasfocus.com';
		objacfBankDetails.password = 'swatisharma10';
		objacfBankDetails.bank_api_name = 'test';
		objacfBankDetails.redirect();
		objacfBankDetails.getAllBank();
		objacfBankDetails.submit();
		objacfBankDetails.selectedAccounts();
		objacfBankDetails.OK();
		objacfBankDetails.loginRequest('bank_of_statements','swati.sharma@saasfocus.com','swatisharma10',true);
    }
    
    static testMethod void validateacfBankDetails1() 
    {
        LoadData();
		acfBankDetails objacfBankDetails = new acfBankDetails();
		objacfBankDetails.user = '';
		objacfBankDetails.password = '';
		objacfBankDetails.bank_api_name = 'test';
		objacfBankDetails.redirect();
		objacfBankDetails.getAllBank();
		objacfBankDetails.submit();
		objacfBankDetails.selectedAccounts();
		objacfBankDetails.OK();
		objacfBankDetails.loginRequest('','','',true);
    }
    
    private static void LoadData()
    {
    	createBankDetail();
    	createlead();
    	createAccount();
    	createContact();
    	createUser();
    	createOpportunity();
    }
    
     static void createBankDetail()
    {
    	objBankDetail = new acfBank_Detail__c();
    	objBankDetail = acfCommontrackerClass.createBankDetail(objBankDetail);
    }
    
    static void createLead()
    {
    	objLead = new Lead();
    	objLead = acfCommontrackerClass.createLead(objLead);
    }
    
    static void createAccount()
    {
    	objAccount = new Account();
    	objAccount = acfCommontrackerClass.createAccount(objAccount);
    }
    
    static void createContact()
    {
    	objContact = new Contact();
    	//objContact.AccountId = objAccount.id;
    	objContact.acf_lead__c = objLead.id;
    	objContact = acfCommontrackerClass.createContact(objContact,objAccount);
    }
    
    static void createuser()
    {
    	objUser = new User();
    	objuser.ContactId = objContact.id;
    	objUser = acfCommontrackerClass.createuser(objUser,objContact,objAccount);
    }
    
    static void createOpportunity()
    {
      objOpportunity = new Opportunity();
      objOpportunity.AccountId = objuser.AccountId;
      objOpportunity.name = 'test';
      objOpportunity.Stagename = 'Application Taken';
      objOpportunity.CloseDate = System.today();
      insert objOpportunity;
    }
}