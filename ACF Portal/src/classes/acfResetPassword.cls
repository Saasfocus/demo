/*
    This class is used to reset password for first time login
    =============================================================================
    Name                             Date                                version
    =============================================================================
    Pawan Mudgal                11/19/2014                                1.0
    =============================================================================
*/

public class acfResetPassword
{
    //variables declarations
    public String stroldPassword {get;set;}
    public String strnewPassword {get;set;}
    public String strverifynewPassword {get;set;}
    public String isSuccess {get;set;}
    Public String pageName {get;set;} 
    Public Boolean IsLoginAttemptCompleted {get;set;}
    List<User> lstUser;
    //Constructor
    public acfResetPassword()
    {
        stroldPassword = '';
        strnewPassword = '';
        strverifynewPassword = '';
        isSuccess = 'False';
        
        string pge = Apexpages.currentPage().getUrl();
        system.debug('pge***'+pge);
        List<String> parts = pge.split('/'); 
        List<String> parts1 = parts[2].split('\\?');
        pageName = parts1[0].toLowerCase(); 
        system.debug('cons pge_name --------'+pageName);   
       /* pageName   = ApexPages.CurrentPage().getUrl(); 
        pageName = pageName.replaceFirst('/apex/',''); 
        pageName = pageName.substring(0,pageName.indexof('?')); 
        system.debug('test' + pageName);
        pageName = pageName.toLowerCase();*/
        lstUser = [select id,Account.acfIsLoginAttemptCompleted__pc,Account.acfPassword__c from user where id=: UserInfo.getuserId()];
        If(lstUser != null && lstUser.size()>0){
            If(lstUser[0].Account != null){
              system.debug('@@@Karthik'+lstUser[0]);
               IsLoginAttemptCompleted = lstUser[0].Account.acfIsLoginAttemptCompleted__pc; 
               If(IsLoginAttemptCompleted == false && pageName != 'clickeditprofile'){
                  stroldPassword = lstUser[0].Account.acfPassword__c;
               }
            }
        }
      }    
    
    public PageReference ResetPassword()
    {
       isSuccess = 'False';
     /*  If(lstUser != null && lstUser.size()>0){
            If(lstUser[0].Account != null){
               system.debug('@@@'+lstUser[0]);
               IsLoginAttemptCompleted = lstUser[0].Account.acfIsLoginAttemptCompleted__pc; 
               If(IsLoginAttemptCompleted == false && pageName != 'clickeditprofile'){
                  stroldPassword = lstUser[0].Account.acfPassword__c;
               }
            }
        }*/
        system.debug('@@@@'+stroldPassword);
        if(stroldPassword != Null && stroldPassword != '' && strnewPassword != Null && strnewPassword != '' && strverifynewPassword != Null && strverifynewPassword != '')
        {
            if(strnewPassword.length() > 8 && strverifynewPassword.length() > 8)
            {
                if(ChkValidPassword(strnewPassword) && ChkValidPassword(strverifynewPassword))
                {
                    try
                    {
                        PageReference chngpass = Site.changePassword(strnewPassword,strverifynewPassword,stroldPassword);
                        
                        if(chngpass != null)
                        {
                            if(pageName == 'clickeditprofile')
                            {
                                User objuser = new User(id=UserInfo.getUserId());  
                                objuser.acfIs_Password_Reset__c = true;
                                update objuser;
                                isSuccess = 'True';
                                return null;
                            }
                            else
                            {
                                String strReturnUrl = acfCommon.sendReturnUrl(UserInfo.getUserId(),false,'acfResetPassword');
                                
                                if(strReturnUrl != 'error')
                                {
                                    User objuser = new User(id=UserInfo.getUserId());  
                                    objuser.acfIs_Password_Reset__c = true;
                                    update objuser;
                                    
                                    string retUrl = acfcommon.sendReturnUrl(objuser.id,false,'acfResetPassword');//acfLoanDashboard'
                                    
                                    PageReference pageref = new PageReference('/partners/'+retUrl);// + strReturnUrl);
                                    pageref.setRedirect(true);
                                    return(pageref);
                                }   
                                else
                                {
                                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error occured. contact administrator...'));  
                                    return null;              
                                }      
                            }
                        }
                        
                        return null;
                    }
                    catch(Exception ex)
                    {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage()));  
                        return null;          
                    }
                }
                else
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Password must be alphanumeric'));  
                    return null;          
                }
            }
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Password must be greater than 8 characters'));  
                return null;      
            }
        }
        else
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please provide values for all the fields'));  
            return null;  
        }
    }
    
    public boolean ChkValidPassword(string strPassword)
    {
        if(!Pattern.matches('(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9!@#$%^&*]{8,20})', strPassword))
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}