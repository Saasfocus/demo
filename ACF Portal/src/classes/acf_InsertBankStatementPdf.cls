public class acf_InsertBankStatementPdf {
    
    public  list<acfStatementResponseDeserialized.Account> lstWrap{get;set;}
    Public string BankSlugname{get;set;}
    Public string username{get;set;}
    Public string password{get;set;}

    public acf_InsertBankStatementPdf()
    {
         BankSlugname = 'bank_of_statements';//Apexpages.currentpage().getparameters().get('BankSlugname');
         username = 'abc@def.com.au';//Apexpages.currentpage().getparameters().get('username');
         password = 'asd123_^&*';//Apexpages.currentpage().getparameters().get('password');
         system.debug('BankSlugname --------- '+BankSlugname+' -- username ----------- '+username+' -- password -------- '+password);
         if(BankSlugname != Null && username != Null)
         {
             /*acResponse.bankSatementResponseSuccessorFailure loginresponse = loginRequest(BankSlugname ,username ,password ,true);
             statementRequest(loginresponse,'003O000000W1zaM',true);
             
             FetchBankStatement(loginresponse,'003O000000W1zaM',true);
             */
         }    
    }
    
    public void TestPdf()
    {
         BankSlugname = 'bank_of_statements';//Apexpages.currentpage().getparameters().get('BankSlugname');
         username = 'abc@def.com.au';//Apexpages.currentpage().getparameters().get('username');
         password = 'asd123_^&*';//Apexpages.currentpage().getparameters().get('password');
         system.debug('BankSlugname --------- '+BankSlugname+' -- username ----------- '+username+' -- password -------- '+password);
         if(BankSlugname != Null && username != Null)
         {
             acResponse.bankSatementResponseSuccessorFailure loginresponse = loginRequest(BankSlugname ,username ,password ,true);
             //statementRequest(loginresponse,'003O000000W1zaM',true);
             
             FetchBankStatement(loginresponse,'003O000000W1zaM',true);
         }    
    }
    
    public void FetchBankStatement(acResponse.bankSatementResponseSuccessorFailure objtoProcess, string leadId, boolean isTest)
    {
    	system.debug('Call::NRAC:::');
    	String user_Token ;
        String AccountId=''; 
        
        if(objtoProcess.success == true)
        {
                user_Token = objtoProcess.response.user_token;
                String Accountsparams = apexpages.currentpage().getparameters().get('Accounts');
                Set<String> setStrs = new Set<String>();
                if(Accountsparams != ''&& Accountsparams != Null )
                {
                    for(String s:Accountsparams.split(','))
                    {
                                setStrs.add(s);
                    }
                }
                
                for(bankStatementResponse.cls_accounts temp: objtoProcess.response.accounts.accounts)
                {
                    if(setStrs.size() > 0)
                    {
                        if(setStrs.contains(temp.id))
                    {
                        AccountId = AccountId + temp.id + ',';
                    }
                    }else
                    {
                        AccountId = AccountId + temp.id + ',';
                    }   
                }
                if(AccountId.lastIndexOf(',') != -1)
                {
                	AccountId = AccountId.substring(0, AccountId.lastIndexOf(','));
                }
                
                system.debug('{"user_token":"'+user_Token+'","accounts":{"bank_of_statements":['+AccountId+']}}');
                
		    	String body = '{"user_token":"'+user_Token+'","accounts":{"bank_of_statements":['+AccountId+']}}';
		   		HttpRequest req = new HttpRequest();
		        req.setEndpoint('https://test.bankstatements.com.au/api/v1/files');
		        req.setMethod('GET');
		        req.setHeader('content-type', 'application/json');
		        req.setHeader('Accept-Language', 'en-US,en;q=0.8;');
		        req.setHeader('Accept-Encoding', 'gzip,deflate,sdch');
		        req.setHeader('Accept', 'application/xml');
		        req.setHeader('X-API-KEY', 'QD6TMQI6MLXHKNV6SLILNZECRGOWFWVUOLELF4HF');
		        //req.setbody(body);
		        
		        Blob headerValue = Blob.valueOf('{"user_token":"'+user_Token+'","accounts":{"bank_of_statements":['+AccountId+']}}');
			    String authorizationHeader = 'BASIC ' +
			    EncodingUtil.base64Encode(headerValue);
			    req.setHeader('Authorization', authorizationHeader);
		        
		        req.setTimeout(50000);
		        Http http = new Http();
		        HTTPResponse res = http.send(req);
		        blob imgFrontResponse = res.getBodyAsBlob(); 
		        System.debug('NRAC::::: - '+imgFrontResponse+':::'+String.valueOf(res.getStatusCode()));
		        
		        Attachment attach_back_img = new Attachment();
	        	attach_back_img.body = imgFrontResponse;
	        	attach_back_img.name = 'front1.zip';
	        	attach_back_img.ParentId = '003O000000WWVHC';
	        	insert attach_back_img;
        }
    }

    public acResponse.bankSatementResponseSuccessorFailure loginRequest(String bank_Slug_Nmme,String username,String password,Boolean istest)
    {
                acResponse.bankSatementResponseSuccessorFailure return_Ref = new acResponse.bankSatementResponseSuccessorFailure(Null,false,Null);
                
                String response;
                if(String.isBlank(String.valueof(bank_Slug_Nmme)))
                {
                response = 'Bank Slug Name is Mandatory.';
                
                return new acResponse.bankSatementResponseSuccessorFailure(Null,false,response);
                }
                if(String.isBlank(String.valueof(username)))
                {
                response = 'User Name is Mandatory.';
                return new acResponse.bankSatementResponseSuccessorFailure(Null,false,response);
                }
                if(String.isBlank(String.valueof(password)))
                {
                response = 'Password is Mandatory.';
                return new acResponse.bankSatementResponseSuccessorFailure(Null,false,response);
                }
                                String body = '{"credentials": {"institution": "'+bank_Slug_Nmme+'","username": "'+username+'","password": "'+password+'"}}';
                                HttpRequest req = new HttpRequest();
                                req.setEndpoint('https://test.bankstatements.com.au/api/v1/login_fetch_all');//req.setEndpoint('https://test.bankstatements.com.au/api/v1/login');
                                req.setMethod('POST');
                                req.setHeader('content-type', 'application/json');
                                req.setHeader('Accept-Language', 'en-US,en;q=0.8');
                                req.setHeader('Accept-Encoding', 'gzip,deflate,sdch');
                                req.setHeader('Accept', '*/*');
                                req.setHeader('X-API-KEY', 'QD6TMQI6MLXHKNV6SLILNZECRGOWFWVUOLELF4HF');
                                req.setbody(body);
                                Http http = new Http();
                                String Statuscode;
                                String responseFromHttp;
                                if(isTest == true)
                                {
                                                try
                                                {
                                                HTTPResponse res = http.send(req);
                                                 Statuscode = String.valueOf(res.getStatusCode());
                                                 responseFromHttp = res.getBody();
                                                 System.debug('@@@@@ - '+responseFromHttp);
                                                 if(Statuscode == '200')
                                                 {
                                                        return new acResponse.bankSatementResponseSuccessorFailure(bankStatementResponse.parse(responseFromHttp),true,'Successful Hit.');
                                                 }else
                                                 {
                                                        return new acResponse.bankSatementResponseSuccessorFailure(Null,false,'Unsuccessful Hit. Response from Server is : '+response);
                                                 }
                                                 
                                                }catch(Exception e)
                                                {
                                                        response = e.getMessage();
                                                        return new acResponse.bankSatementResponseSuccessorFailure(Null,false,response);
                                                }
                                }else
                                {
                                        Statuscode = '200';
                                        response = 'Test Mode';
                                        return new acResponse.bankSatementResponseSuccessorFailure(Null,true,response);
                                }
                                
                
                return return_Ref;
                
        }


public  acResponse.bankStatemtnResponseStatemtnsresponse statementRequest(acResponse.bankSatementResponseSuccessorFailure objtoProcess,Id ConId,Boolean isTest)
        {
                String user_Token ;
                String AccountId='';
                
                if(objtoProcess.success == true)
                {
                        user_Token = objtoProcess.response.user_token;
                        String Accountsparams = apexpages.currentpage().getparameters().get('Accounts');
                        Set<String> setStrs = new Set<String>();
                        if(Accountsparams != ''&& Accountsparams != Null )
                        {
                            for(String s:Accountsparams.split(','))
                            {
                                        setStrs.add(s);
                            }
                        }
                        
                        for(bankStatementResponse.cls_accounts temp: objtoProcess.response.accounts.accounts)
                        {
                            if(setStrs.size() > 0)
                            {
                                if(setStrs.contains(temp.id))
                            {
                                AccountId = AccountId + temp.id + ',';
                            }
                            }else
                            {
                                AccountId = AccountId + temp.id + ',';
                            }   
                        }
                        if(AccountId.lastIndexOf(',') != -1)
                        {
                        AccountId = AccountId.substring(0, AccountId.lastIndexOf(','));
                        }
                        
                        
                        String body = '{"user_token":"'+user_Token+'","accounts":{"'+BankSlugname+'":['+AccountId+']}}';
                        HttpRequest req = new HttpRequest();
                        req.setEndpoint('https://test.bankstatements.com.au/api/v1/statements');
                        req.setMethod('POST');
                        req.setHeader('content-type', 'application/json');
                        req.setHeader('Accept-Language', 'en-US,en;q=0.8;');
                        req.setHeader('Accept-Encoding', 'gzip,deflate,sdch');
                        req.setHeader('Accept', 'application/xml');
                        req.setHeader('X-API-KEY', 'QD6TMQI6MLXHKNV6SLILNZECRGOWFWVUOLELF4HF');
                        System.debug('!!!!Statement Body -'+body);
                        req.setbody(body);
                        req.setTimeout(50000);
                        Http http = new Http();
                        HTTPResponse res = http.send(req);
                        String Statuscode = String.valueOf(res.getStatusCode());
                        String response = res.getBody();
                        System.debug('!!!! XML Response - '+response);
                        //parsing the document
                        Dom.Document doc = new Dom.Document();
            doc.load(response);
            Dom.XMLNode resp = doc.getRootElement();
            Dom.XMLNode Accounts = resp.getChildElement('accounts', null);
            Dom.XMLNode BankStatements = Accounts.getChildElement('bank_of_statements', null);
            Dom.XMLNode BankStatements_Account = BankStatements.getChildElement('accounts', null);
            lstWrap = new list<acfStatementResponseDeserialized.Account>();
            for (Dom.XMLNode AccInfo: BankStatements_Account.getChildElements()) {
                         acfStatementResponseDeserialized.Account obj = new  acfStatementResponseDeserialized.Account();
                     obj.accountHolder = AccInfo.getChildElement('accountHolder',Null).getText();
                     obj.name = AccInfo.getChildElement('name',Null).getText();
                     obj.id = AccInfo.getChildElement('id',Null).getText();
                     obj.bsb = AccInfo.getChildElement('bsb',Null).getText();
                     obj.available = AccInfo.getChildElement('available',Null).getText();
                     obj.accountNumber = AccInfo.getChildElement('accountNumber',Null).getText();
                     obj.balance = AccInfo.getChildElement('balance',Null).getText();
                     Dom.XMLNode StatementData = AccInfo.getChildElement('statementData',Null);
                     acfStatementResponseDeserialized.StatementData statementDatatemp  = new acfStatementResponseDeserialized.StatementData(StatementData);
                     obj.statementData   = statementDatatemp;
                     lstWrap.add(obj);  
                 }
           for(acfStatementResponseDeserialized.Account tempref : lstWrap)
           {
                 System.debug('@@@@ statementData -'+ tempref.statementData);
           }
          
                        return null;
                }
                return null;
                  
                       
        }


}