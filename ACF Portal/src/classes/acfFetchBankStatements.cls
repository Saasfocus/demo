/*
    This class is used to fetch the bank statements
    =================================================
    Name                 Date                version
    =================================================
    Prashant Kumar     12/11/2014                1.0
    =================================================
*/
Public class acfFetchBankStatements
{
    //Constructor
    public void acfFetchBankStatements(){}
    
    /*
        This method is used to get User Token from Bankstatements 
        ========================================================
        Name                     Date                version
        ========================================================
        Prashant Kumar         12/11/2014                1.0
        ========================================================
    */
    public void FetchInstitutionDetails(String strBankName)
    {
        //fetch the login details from custom settings
        acfBank_Statement_Credential__c objBankStatement = acfBank_Statement_Credential__c.getValues('Credential');
         
        //create http request
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://test.bankstatements.com.au/api/v1/institutions?X-API-KEY=QD6TMQI6MLXHKNV6SLILNZECRGOWFWVUOLELF4HF');
        req.setHeader('content-type', 'application/json');
        req.setMethod('GET');
        
        //getting Response     
        Http http = new Http();
        HTTPResponse objresponse = http.send(req); 
        system.debug('objresponse?????' + objresponse.getBody()); 
    }
    
    public void initiate()
    {
    	FetchUserToken('bank_of_statements', 'abc@def.com.au', 'asd123_^&*','1','003O000000W1zaM');
    }
    
    /*
        This method is used to get User Token from Bankstatements 
        ========================================================
        Name                     Date                version
        ========================================================
        Prashant Kumar         12/11/2014                1.0
        ========================================================
    */
    public void FetchUserToken(String strInstitution, string strUsername, string strPassword, string strSilent, string strLeadId)
    {
        if(strInstitution != null && strInstitution != '' && strUsername != null && strUsername != '' &&
            strPassword != null && strPassword != '' && strSilent != null && strSilent != '')
        {    
            //fetch the login details from custom settings
            acfBank_Statement_Credential__c objBankStatement = acfBank_Statement_Credential__c.getValues('Credential');
            
            //Creating json Body
            JSONGenerator objJsonGen = JSON.createGenerator(true);
            objJsonGen.writeStartObject();     
            objJsonGen.writeFieldName('credentials');
            objJsonGen.writeStartObject();    
            objJsonGen.writeStringField('institution', strInstitution);
            objJsonGen.writeStringField('username', strUsername);
            objJsonGen.writeStringField('password', strPassword);
            objJsonGen.writeStringField('Silent?', strSilent);
            objJsonGen.writeEndObject();
            objJsonGen.writeEndObject();
            
            // Get the JSON string.             
            String jsonRequest = objJsonGen.getAsString();
            system.debug('jsonRequest???' + jsonRequest);
                        
            //create http request
            HttpRequest req = new HttpRequest();
            req.setEndpoint(objBankStatement.acfEndPoint_Url__c + '/login_fetch_all');
            req.setMethod('POST');
            req.setHeader('content-type', 'application/json');
            req.setHeader('X-API-KEY', objBankStatement.acfX_API_KEY__c);
            req.setTimeout(100000);
            req.setBody(jsonRequest);
            
            //getting Response     
            Http http = new Http();
            HTTPResponse jsonresponse = http.send(req); 
            String jsonResponseBody = jsonresponse.getBody();
            
            list<string> lstSplitWithUserToken =  jsonResponseBody.split('user_token=');
            list<string> lstSplitWithAccount = lstSplitWithUserToken[1].split('accounts=');
            
            string strFinalUserToken = lstSplitWithAccount[0].replaceAll('\'','');
            system.debug('str???' + strFinalUserToken);
            
            //FetchInstitutionDetails('test');
            
            //Call Fetch files 
            FetchFile(strFinalUserToken, strLeadId);
            
        }
    }    
    
    /*
        This method is used to get File from Bankstatements 
        ========================================================
        Name                     Date                version
        ========================================================
        Prashant Kumar         12/11/2014                1.0
        ========================================================
    */
    public void FetchFile(string UserToken, string strLeadId)
    {
        //fetch the login details from custom settings
        acfBank_Statement_Credential__c objBankStatement = acfBank_Statement_Credential__c.getValues('Credential');
         
        //create http request
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://test.bankstatements.com.au/api/v1/files?X-API-KEY=QD6TMQI6MLXHKNV6SLILNZECRGOWFWVUOLELF4HF&user_token='+ UserToken);
        req.setMethod('GET');
        
        //getting Response     
        Http http = new Http();
        HTTPResponse objresponse = http.send(req); 
        system.debug('objresponse?????' + objresponse.getBody()); 
        
        Blob objBlob = objresponse.getBodyAsBlob();
        
        system.debug('status???' + objresponse.getBodyAsBlob());
        
        if(strLeadId != null && strLeadId != '')
        {
            list<Lead> lstLead = [select id, IsConverted, ConvertedOpportunityId from Lead where id =:strLeadId];
            
            if(lstLead != null && lstLead.size() > 0)
            {
                if(lstLead[0].IsConverted == true)
                {
                    list<Opportunity> lstOpportunity = [select Id , 
                                                        (select id,acf_Bank_Statement_Doc__c from Required_Documents__r where acf_Bank_Statement_Doc__c = true)
                                                        from Opportunity where Id =: lstLead[0].ConvertedOpportunityId];
                    
                    if(lstOpportunity != null && lstOpportunity.size() > 0)
                    {
                       if(lstOpportunity[0].Required_Documents__r != null && lstOpportunity[0].Required_Documents__r.size() > 0)
                       {
                            Attachment attach = new Attachment();
                            attach.Body = objBlob;
                            attach.Name = 'Bankstatement.zip';
                            attach.ContentType = 'application/zip';
                            attach.ParentID = lstOpportunity[0].Required_Documents__r[0].id;
                            insert attach;                                        
                       }     
                    }
                }
                else
                {
                    list<Lead> lstLeadTemp = [select id, 
                                                (select id,acf_Bank_Statement_Doc__c from Required_Documents__r where acf_Bank_Statement_Doc__c = true)
                                                from Lead where id =:strLeadId];
                    
                    if(lstLeadTemp != null && lstLeadTemp.size() > 0)
                    {
                         if(lstLeadTemp[0].Required_Documents__r != null && lstLeadTemp[0].Required_Documents__r.size() > 0)
                         {
                            Attachment attach = new Attachment();
                            attach.Body = objBlob;
                            attach.Name = 'Bankstatement.zip';
                            attach.ContentType = 'application/zip';
                            attach.ParentID = lstLeadTemp[0].Required_Documents__r[0].id;
                            insert attach;                                         
                         }   
                    }                        
                }
            }
        }
        
        
    }
}