public class acfJumioIdentityVerificationController 
{
   transient Attachment attach;
   transient Attachment docattach;
   public List<selectOption>lstIdntificationType{get;set;}
   public boolean IsshowIdentityType{get;set;}
   public boolean IsshowIdentityUpload{get;set;}
   public boolean IsshowPhotoUpload{get;set;}
   public string strIdentificationType{get;set;}
   public blob IdentityBlob; 
   public Id LoggedInContact;
   public Id LoggedInLeadId;
   public string strmsg{get;set;}
   public Attachment getdocattach()
   {
    if(docattach== null)
        docattach = new Attachment();
     return docattach;
   }
   public Attachment getattach()
   {
    if(attach== null)
        attach= new Attachment();
     return attach;
   }
   public acfJumioIdentityVerificationController ()
   {
       IsshowIdentityType   = true;
       IsshowIdentityUpload = false;
       IsshowPhotoUpload    = false;
       //IdentityBlob = new blob();
       attach= new Attachment();
       docattach = new Attachment();
       LoggedInLeadId  = acfCommon.getCurrentLoggedInLeadId(UserInfo.getUserId());            
        lstIdntificationType = new List<selectOption>();    
       //LoggedInLeadId  =  '00QO0000002QAJ4';    
       lstIdntificationType.add(new SelectOption('Passport','Passport'));
       lstIdntificationType.add(new SelectOption('Driving Licence','Driving Licence'));
       lstIdntificationType.add(new SelectOption('Identity','Identity'));
   }
   
   public void btnContinue()
   {
      strmsg = '';
      if(IsshowIdentityType == true)
      {
        if(strIdentificationType <> null && strIdentificationType <> '' && strIdentificationType <> 'NONE')
        { 
            IsshowIdentityUpload = true;
            IsshowPhotoUpload    = false;
            IsshowIdentityType = false;
        }
        else
        {
          strmsg = 'Please select Identity Type';
        }   
      }
      else if(IsshowIdentityUpload == true)
      {
        IdentityBlob = attach.body;
        IsshowIdentityUpload = false;
        IsshowPhotoUpload    = true;
        IsshowIdentityType = false;
      }
      else if(IsshowPhotoUpload  == true)
      {
        IsshowIdentityUpload = false;
        IsshowPhotoUpload    = false;
        IsshowIdentityType   = true;
      } 
   }
   
   public pageReference UploadDoc()
   {
     strmsg = '';
     if(attach.body <> null && docattach.body <> null)
     {
         system.debug('@#$%'+attach.body+ '$$@#$%%' + docattach.body );
         try
           {       
              string jsonRequest;
             jsonRequest = '{"merchantIdScanReference":"1234566eeeeABCDEDFRT","frontsideImage":"'+EncodingUtil.base64Encode(attach.body)+'","faceImage":"'+EncodingUtil.base64Encode(docattach.body)+'"}';
             Attachment attach = new Attachment();
             pageReference pgReturn = UploadJumioDocument(jsonRequest);
             return pgReturn;
           }
           catch(Exception Ex)
           {
           }
           finally{
              attach= new Attachment();
              docattach = new Attachment();
           }
      }
      else
      {
        strmsg = 'Please Select both document';
      }     
       return null;  
   }
   
   public pageReference UploadJumioDocument(string jsonRequest)
   {
         //Create Http request
        Jumio_Credentials__c objJumioCredential = Jumio_Credentials__c.getValues('Credential');       
        
        String strUserName = objJumioCredential.Merchant_API_token__c;
        String strPassword = objJumioCredential.Active_API_secret__c;    
        String strEndPoint = objJumioCredential.EndPoint__c;
        String strAuthorizationHeader = 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(strUserName + ':' + strPassword)); 
        
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint('https://netverify.com/api/netverify/v1/performNetverify');
        System.debug('!!!! strAuthorizationHeader -'+ strAuthorizationHeader);
        req.setHeader('Authorization', strAuthorizationHeader);
        req.setHeader('Accept', 'application/json');             
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('User-Agent', 'SaasFocus Saas/1.9.0');
        req.setBody(jsonRequest);
        
        //send request        
        Http http = new Http();
        HTTPResponse jsonResponse = http.send(req);
        String jsonResponseBody = jsonResponse.getBody(); 
        system.debug('@@@json'+jsonResponseBody);  
        
        RootObject objRootObject = parse(jsonResponseBody);
        
        List<lead> lstLead = [select id, IsConverted from Lead where id =: LoggedInLeadId];
        if(lstLead != null && lstLead.size() > 0)
        {
            if(!lstLead[0].IsConverted && objRootObject.jumioIdScanReference <> null)
            {
                   List<Required_Document__c>lstRequiredDoc = [select id,acfStatus__c,acf_Reference_No__c,acfDocument_Master__c from Required_Document__c where acfDocument_Master__c <> null and Lead__c=:LoggedInLeadId and acfDocument_Master__r.acf_Source__c = 'Jumio' limit 1];
                   if(lstRequiredDoc <> null && lstRequiredDoc.size()>0)
                   {
                       lstRequiredDoc[0].acfStatus__c = 'Pending';
                       lstRequiredDoc[0].acf_Reference_No__c = objRootObject.jumioIdScanReference;
                       update lstRequiredDoc;
                   }
                   lstLead[0].acfIs_Identity_Document_Submitted__c = true;
                   update lstLead;
                   string retPgName = acfCommon.sendReturnUrl(Userinfo.getuserID(),false,'acfJumioIdentityVerification'); 
                   system.debug('@@#$%%'+retPgName);
                   return new pageReference('/'+retPgName);
            }
        }
       
       return null;
   }
   
    public class RootObject
    {
        public string timestamp { get; set; }
        public string authorizationToken { get; set; }
        public string clientRedirectUrl { get; set; }
        public string jumioIdScanReference { get; set; }
    }
    
    public static RootObject parse(String json) 
    {
        return (RootObject) System.JSON.deserialize(json, RootObject.class);
    }   
}