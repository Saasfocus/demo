public class acfTestPage 
{
    public acfTestPage()
    {
        
    }
    
    @RemoteAction
    public static List<AutoCompleteBank> AutoCompleteBanklList(string strFilter)
    {
        List<AutoCompleteBank> lstBankList = new List<AutoCompleteBank>();
        
        List<acfBank_Detail__c> lstBank = [select id,acfBank_Name__c,acfImage_URL__c,acfAttachmentId__c from acfBank_Detail__c where acfBank_Name__c like: '%'+strFilter+'%'];
        
        if(lstBank != null && lstBank.size() > 0)
        {
            for(acfBank_Detail__c objBank : lstBank)
            {
                lstBankList.add(new AutoCompleteBank(objBank.id, objBank.acfBank_Name__c));
            }
        }
        
        return lstBankList;
    }
    
    public class AutoCompleteBank
    {
        public String id;
        public String text;
        
        public AutoCompleteBank(string id, string text){
            this.id = String.valueOf(id);
            this.text = String.valueOf(text);
        }
    }
}