/*
    This class is used to reset password if the user forgots his/her password.
    =============================================================================
    Name                             Date                                version
    =============================================================================
    Pawan Mudgal                11/27/2014                                1.0
    =============================================================================
*/

public class acfForgotPassword
{
    //variables declarations
    public String strUsername {get;set;}
    public Boolean mailSent {get;set;}
    public User objuser {get;set;}
    public List<User> lstUser{get;set;}
        
    //Constructor
    public acfForgotPassword()
    {
        strUsername = '';
        mailSent = false;
        objuser = new User();
        lstUser = new List<User> ();
    }    
    
    public void ResetPassword()
    {
        mailSent = false;
        
        if(strUsername != '')
        {
            lstUser = [select Id,Name,IsActive,Email,MobilePhone from user where Username =: strUsername];
            System.debug('Swati' +lstUser);
            if(lstUser != Null && lstUser.size() > 0)
            {
                objuser = lstUser[0];
                
                if(objuser.IsActive)
                {
                    
                    try
                    {
                        Boolean emailsent = Site.forgotPassword(strUsername);
                        system.debug('emailsent'+emailsent);
                        if(emailsent)
                        {
                            String strMessage = 'A link has been sent to your email ID ('+objuser.Email+'). Please follow the link to reset your password.';
                            mailSent = true;
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Password reset mail sent successfully.'));    
                            String response = acfCommon.sendSMS(objuser.Name,objuser.MobilePhone,strMessage,true);
                        }
                        else
                        {
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error in reseting password.'));  
                        }
                    }
                    catch(Exception ex)
                    {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage()));  
                    }
                }
                else
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'This user is temporary disabled or not an active user.'));  
                }
            }
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Invalid Username.'));  
            }
        }
        else
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please provide value for username.'));  
        }
    }
}