public  class acfTriggerOnOpportunityLineItemHandler 
{
  public void OnBeforeInsert(List<Opportunitylineitem>triggerNew)
  {
    UpdateOpportunityLineItem(triggerNew);
  }
  public void OnBeforeUpdate(List<Opportunitylineitem>triggerNew)
  {
    UpdateOpportunityLineItem(triggerNew);
  }
  public void OnAfterInsert(List<Opportunitylineitem>triggerNew)
  {
    
    UpdateFeeInOpportunity(triggerNew);
  }
  public void OnAfterUpdate(List<Opportunitylineitem>triggerNew)
  {
    UpdateFeeInOpportunity(triggerNew);
  }
  public void UpdateFeeInOpportunity(List<Opportunitylineitem>triggerNew)
  {
    set<Id>setOpportunityID = new set<Id>();
    List<Opportunity>lstOpportunityToUpdate = new List<Opportunity>();
    for(Opportunitylineitem objOppLi:triggerNew)
    { 
      setOpportunityID.add(objOppLi.OpportunityId);
    }
    system.debug('!@#$%'+setOpportunityID);
    if(setOpportunityID.size()>0)
    {
      decimal TotalApplicationFee,TotalValuationFee,TotalLegalFee,TotalOnGoingFee,TotalCurrentRate,TotalComparisionRate;
      for(Opportunity opp:[select id,Car_Application_Fee__c,acfOngoing_Fees__c,acfValuation_Fee__c,acfLegal_Fees__c,Current_Interest_Rate__c,acfComparison_Rate__c, 
      acfProduct_LVR__r.acfProduct__r.acfAdd_Repayments__c,acfProduct_LVR__r.acfProduct__r.acfInterest_Only__c,acfProduct_LVR__r.acfProduct__r.acfDebit_Card__c,
                 acfProduct_LVR__r.acfProduct__r.acfOffset_Account__c,acfProduct_LVR__r.acfProduct__r.acfRedraw__c,acfProduct_LVR__r.acfProduct__r.acfMultiple_Splits__c,               
                 (select id,acfValuation_Fee__c,acfOngoing_Fees__c,acfLegal_Fees__c,acfApplication_Fee__c,acfRate__c,acfComparison_Rate__c
                 from OpportunityLineItems) from Opportunity where id in:setOpportunityID])
      {
        TotalApplicationFee = 0.00;
        TotalValuationFee = 0;
        TotalLegalFee = 0;
        TotalOnGoingFee = 0;
        TotalCurrentRate = 0;
        TotalComparisionRate = 0;
        for(OpportunityLineItem OppLi:opp.OpportunityLineItems)
        {
          if(OppLi.acfApplication_Fee__c <> null)
            TotalApplicationFee += OppLi.acfApplication_Fee__c;
          if(OppLi.acfValuation_Fee__c <> null)  
            TotalValuationFee   += OppLi.acfValuation_Fee__c;
          if(OppLi.acfLegal_Fees__c <> null)  
            TotalLegalFee     += OppLi.acfLegal_Fees__c;
          if(OppLi.acfOngoing_Fees__c <> null)  
            TotalOnGoingFee     += OppLi.acfOngoing_Fees__c;
          if(OppLi.acfRate__c <> null)  
            TotalCurrentRate     += OppLi.acfRate__c;
          if(OppLi.acfComparison_Rate__c <> null)   
            TotalComparisionRate += OppLi.acfComparison_Rate__c;    
        }
        opp.Car_Application_Fee__c      = TotalApplicationFee;
        opp.acfValuation_Fee__c         = TotalValuationFee;
        opp.acfLegal_Fees__c            = TotalLegalFee;
        opp.acfOngoing_Fees__c          = TotalOnGoingFee;
        opp.Current_Interest_Rate__c    = TotalCurrentRate;
        opp.acfComparison_Rate__c       = TotalComparisionRate;
        lstOpportunityToUpdate.add(opp);
      }
      system.debug('!@#$%'+lstOpportunityToUpdate);
      if(lstOpportunityToUpdate.size()>0)
      {
        update lstOpportunityToUpdate;
      }           
    }
  }
  public void UpdateOpportunityLineItem(List<Opportunitylineitem>triggerNew)
  {
    set<Id>setOpportunityId = new set<Id>();
    set<Id>setPriceBookEntryId = new set<Id>();
    set<Id>setProductId = new set<Id>();
    map<Id,PricebookEntry>mapPBEIdToPBE = new map<Id,PricebookEntry>();
    map<Id,Opportunity>mapOppIdToOpp = new map<Id,Opportunity>();
    map<Id,List<LVR_Rate__c>>mapProductIdToListOfLVR = new map<id,List<LVR_Rate__c>>();
    if(triggerNew <> null && triggerNew.size()>0)
    {
      for(OpportunityLineItem objOppLi:triggerNew)
      {
        if(objOppLi.OpportunityId <> null)
        {
           setOpportunityId.add(objOppLi.OpportunityId);
        }
        if(objOppLi.PricebookEntryId <> null)
        {
          setPriceBookEntryId.add(objOppLi.PricebookEntryId);
        }
        system.debug('1234567'+setPriceBookEntryId);
      }
    }
    if(setPriceBookEntryId <> null && setPriceBookEntryId.size()>0)
    {
      for(PricebookEntry objPBE:[select id,product2Id,Product2.acfApplication_Fee__c,
                        product2.acfTenure_in_Years__c,product2.acfvarTenure_In_Years__c,Product2.RecordType.Name,Product2.acfValuation_Fee__c,Product2.ACF_Comparison_Rate__c,
                        Product2.acfOngoing_Fees__c,Product2.acfLegal_Fees__c,Product2.Family from PricebookEntry where id in:setPriceBookEntryId])
      {
        setProductId.add(objPBE.product2Id);
        mapPBEIdToPBE.put(objPBE.id,objPBE);
      }                  
    }
    if(setOpportunityId <> null && setOpportunityId.size()>0)
    {
      for(Opportunity objOpp:[select id,acfCurrentLoanAmount__c,acfCuurent_Rate__c,acfPayment_Type__c,Account.acfApproximately_how_much_is_your_house__pc,
                              acfProduct_LVR__r.acfApplication_Fee__c,acfProduct_LVR__r.ACF_Comparison_Rate__c,acfProduct_LVR__r.acfValuation_Fee__c,acfProduct_LVR__r.acfLegal_Fees__c,
                              acfProduct_LVR__r.acfOngoing_Fees_annual__c from Opportunity where id in:setOpportunityId])
      {
        mapOppIdToOpp.put(objOpp.id,objOpp);            
      }
    }
    if(setProductId <> null && setProductId.size()>0)
    {
      for(LVR_Rate__c objLVRRate:[select id,acfLVR__c,acfProduct__c,acfRate__c from LVR_Rate__c where acfProduct__c in:setProductId])
      {
        if(mapProductIdToListOfLVR.get(objLVRRate.acfProduct__c) == null)
          mapProductIdToListOfLVR.put(objLVRRate.acfProduct__c,new List<LVR_Rate__c>());
        mapProductIdToListOfLVR.get(objLVRRate.acfProduct__c).add(objLVRRate);
      }
    }
    if(mapOppIdToOpp <> null && mapPBEIdToPBE <> null)
    {
      for(OpportunityLineItem objOppLi:triggerNew)
      {

        if(mapOppIdToOpp.get(objOppLi.OpportunityId)<> null && mapPBEIdToPBE.get(objOppLi.PricebookEntryId)<>null && mapProductIdToListOfLVR.get(mapPBEIdToPBE.get(objOppLi.PricebookEntryId).Product2Id)<>null)
        {
          if(mapOppIdToOpp.get(objOppLi.OpportunityId).acfCurrentLoanAmount__c <> null)
          {
            List<decimal>lstFinalLVR = new List<decimal>();
            string PaymentType = (mapOppIdToOpp.get(objOppLi.OpportunityId).acfPayment_Type__c);
            decimal acfRate;
            decimal TenureInYears;
            If(mapPBEIdToPBE.get(objOppLi.PricebookEntryId).Product2.RecordType.Name == 'Fixed'){
              TenureInYears = decimal.valueof(mapPBEIdToPBE.get(objOppLi.PricebookEntryId).Product2.acfTenure_in_Years__c);
            }else If(mapPBEIdToPBE.get(objOppLi.PricebookEntryId).Product2.RecordType.Name == 'Variable'){
              TenureInYears = mapPBEIdToPBE.get(objOppLi.PricebookEntryId).Product2.acfvarTenure_In_Years__c;
            }
              
            decimal HouseValue = (mapOppIdToOpp.get(objOppLi.OpportunityId).Account.acfApproximately_how_much_is_your_house__pc);
            decimal LoanAmount = (mapOppIdToOpp.get(objOppLi.OpportunityId).acfCurrentLoanAmount__c);
            map<decimal,decimal>mapLVRToRate  = new map<decimal,decimal>();
            if(LoanAmount <> null && HouseValue <> Null && TenureInYears <> null && PaymentType <> null)
            {
              Double  LVR = ((LoanAmount/HouseValue)*100);
              system.debug('@#$%%'+LVR);
              List<LVR_Rate__c>lstLVR = mapProductIdToListOfLVR.get(mapPBEIdToPBE.get(objOppLi.PricebookEntryId).Product2Id);
              system.debug('@#$%%'+lstLVR); 
              if(lstLVR <> null && lstLVR.size()>0)
              {
                for(LVR_Rate__c objLVR:lstLVR)
                {
                    if(objLVR.acfLVR__c>=LVR)  
                    {
                      mapLVRToRate.put(objLVR.acfLVR__c,objLVR.acfRate__c);
                      lstFinalLVR.add(objLVR.acfLVR__c);
                    }
                }
                system.debug('testing@@@2'+lstFinalLVR);
                if(lstFinalLVR <> null && lstFinalLVR.size()>0)
                {
                  lstFinalLVR.sort();
                  acfRate = mapLVRToRate.get(lstFinalLVR[0]);
                  if(acfRate <> null && LoanAmount <> null && PaymentType <> null && TenureInYears <> null)
                  {
                    Decimal CurrentEMI = acfCommon.calculateMonthlyPayment(LoanAmount,double.valueof(acfRate),PaymentType,double.valueof(TenureInYears));
                    if(CurrentEMI <> null)
                    {
                       objOppLi.UnitPrice = CurrentEMI;
                       objOppLi.acfRate__c = acfRate;
                       objOppLi.acfComparison_Rate__c = mapOppIdToOpp.get(objOppLi.OpportunityId).acfProduct_LVR__r.ACF_Comparison_Rate__c;
                       objOppLi.Quantity  = TenureInYears*12;
                       objOppLi.acfApplication_Fee__c = mapOppIdToOpp.get(objOppLi.OpportunityId).acfProduct_LVR__r.acfApplication_Fee__c;
                       objOppLi.acfValuation_Fee__c = mapOppIdToOpp.get(objOppLi.OpportunityId).acfProduct_LVR__r.acfValuation_Fee__c;
                       objOppLi.acfLegal_Fees__c = mapOppIdToOpp.get(objOppLi.OpportunityId).acfProduct_LVR__r.acfLegal_Fees__c;
                       objOppLi.acfOngoing_Fees__c = mapOppIdToOpp.get(objOppLi.OpportunityId).acfProduct_LVR__r.acfOngoing_Fees_annual__c;
                    }
                  }
                }
              }
            }     
          }  
        }   
      }
    }  
  }
}