<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <label>Hermes.Cloud Supervision</label>
    <tab>standard-Chatter</tab>
    <tab>standard-Contact</tab>
    <tab>Campaign__c</tab>
    <tab>Supervision</tab>
    <tab>CTI</tab>
    <tab>Call__c</tab>
    <tab>Training</tab>
    <tab>Recorded_Files</tab>
    <tab>acfBank_Detail__c</tab>
    <tab>acfContentManagementSystem__c</tab>
    <tab>Question__c</tab>
    <tab>Script__c</tab>
    <tab>acfTask_Master__c</tab>
    <tab>Dependent_Document__c</tab>
    <tab>Document_Master__c</tab>
    <tab>Required_Document__c</tab>
    <tab>acfDependent_Product__c</tab>
    <tab>acfSuggested_Product__c</tab>
    <tab>acfCMS_Page__c</tab>
</CustomApplication>
